/*
 * RTOS.c
 *
 * Created: 11.03.2014 22:11:35
 *  Author: Asus
 */ 

#include "task_mngr.h"
#include "unnet.h"

// Port B
#define UNET_PORT PORTB
#define UNET_PORT_RO PINB
#define UNET_DDR DDRB
#define UNET_PIN 1
// Port C
#define WATER_SENS_ALARM 5
#define WATER_SENS_ALARM_C 4
// Port D
#define LED_DDR DDRD
#define LED_PORT PORTD
#define RGB_LED_GREEN 7
#define RGB_LED_BLUE 5
#define RGB_LED_RED 6
//

#define LED_FAIL RGB_LED_RED
#define UNET_INTERVAL_MS 1
#define UNET_ADDR 0x03

void DiodeOff(void);

static volatile t_task tt;

//RTOS Interrupt
ISR(RTOS_ISR)
{
	//Enable_Interrupt
	TimerService_isr();  //
}

inline INLINE void SwonLED(uint8_t led) {
	LED_DDR |= (1 << led);	 	/* логический ноль  */
}

inline INLINE void SwoffLED(uint8_t led) {
	LED_DDR &= ~(1 << led);		// режим Hi-Z
}

inline INLINE void InitAll(void)
{
	// задаем питание RGB-светодиоду
//	LED_PORT |= 1 << RGB_LED_GREEN;		// режим pull-up
//	LED_PORT |= 1 << RGB_LED_BLUE;		// режим pull-up
//	LED_PORT |= 1 << RGB_LED_RED;		// режим pull-up
	PORTC |= 1 << WATER_SENS_ALARM;     // pull-up
	PORTC |= 1 << WATER_SENS_ALARM_C;   // pull-up
	DDRC &= ~(1<< WATER_SENS_ALARM);
	DDRC &= ~(1<< WATER_SENS_ALARM_C);
}

void unet_handlr(const t_unet_pckt *ppckt) {
	t_unet_pckt pckt;
	switch(ppckt->cmd) {
	case C_PING_RQ:			// запрос пинг
		// формируем ответное сообщение
		pckt.addr_dst = ppckt->addr_src;
		pckt.addr_src = UNET_ADDR;
		pckt.cmd = C_PING_RP;
		if(!unet_send(ppckt->addr_src, &pckt.cmd, 1))
		{
			if(!unet_enque(&pckt))
			{
				// TODO
			}
		}
	break;
	case C_PING_RP:			// ответ пинг

	break;
	case C_ACCPT:
		// TODO
	break;
	case C_ALM_W:			// датчик протечек тревога

	break;
	case C_DATA:
	break;
	case C_NONE:
	default:
	break;
	}
}

void DiodeOn(void) {
	SwonLED(RGB_LED_GREEN);
	tt.task = DiodeOff;
	SetTimerTask(tt, 1000);
}

void DiodeOff(void) {
	SwoffLED(RGB_LED_GREEN);
	tt.task = DiodeOn;
	SetTimerTask(tt, 1000);
}

void CheckWaterSensorsTask(void) {
	if(!(PINC & (1 << WATER_SENS_ALARM)))
	{
		uint8_t b = C_ALM_W;
		SwonLED(LED_FAIL);
		unet_send(UNET_BROADCAST_ADDR, &b, 1);
	}
	tt.task = CheckWaterSensorsTask;
	SetTimerTask(tt, 100);
}

void PingTask(void) {
	uint8_t b = C_PING_RQ;
	if(!unet_send(0x02, &b, 1)) {
		tt.task = PingTask;
		SetTimerTask(tt, 10000);
	}
}

void DevicePresentsTask(void) {
	t_unet_pckt pckt;
	pckt.addr_dst = UNET_BROADCAST_ADDR;
	pckt.addr_src = UNET_ADDR;
	pckt.cmd = C_PING_RP;
	if(!unet_send(pckt.addr_dst, &pckt.cmd, 1)) {
		if(!unet_enque(&pckt)) {
			tt.task = DevicePresentsTask;
			SetTimerTask(tt, 10000);
		}
	}
}

void UnNetTask(void) {
	tt.task = UnNetTask;
	UnNetTimerHandle();
	SetTimerTask(tt, UNET_INTERVAL_MS);
	if((!(UNET_PORT_RO & (1<<UNET_PIN))))
		SwonLED(RGB_LED_BLUE);
	else
		SwoffLED(RGB_LED_BLUE);
}

//RTOS
inline INLINE void RunRTOS(void)
{
	// Run Timers
	TCCR0A = 1<<WGM01;				// CTC Mode
									// Автосброс после достижения регистра сравнения
	TCCR0B = 1<<CS01;				// Prescaler: Freq = CK/64 -
	TCCR0B |= 1<<CS00;
	TCNT0 = 0;						// Установить начальное значение счётчиков
	OCR0A  = (TimerDivider); 		// Установить значение в регистр сравнения
	TIMSK0 |= 1<<OCIE0A;			// Разрешаем прерывание RTOS - запуск ОС

	Enable_Interrupt
}

int main(void)
{
	InitAll();
	InitRTOS();			//
	RunRTOS();			//

	// WaterSensorAlarm task
	tt.task = CheckWaterSensorsTask;
	SetTimerTask(tt, 100);
	// Diod task
	tt.task = DiodeOn;
	SetTimerTask(tt, 1000);
	// UnNet task
	tt.task = UnNetTask;
	if(unet_init(UNET_PIN, &UNET_PORT, &UNET_PORT_RO, &UNET_DDR, 
				UNET_INTERVAL_MS, UNET_ADDR, unet_handlr))
		SetTimerTask(tt, UNET_INTERVAL_MS);
	else
		SwonLED(LED_FAIL);
	// ping unet
	tt.task = DevicePresentsTask;
	SetTimerTask(tt, 1000);
	
    while(1) 		// Главный цикл диспетчера
	{
		// wdt_reset();	// Сброс собачьего таймера
		TaskManager();	// Вызов диспетчера
	}
}