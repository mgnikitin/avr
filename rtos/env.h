#ifndef _ENV_
#define _ENV_

#include <avr/io.h>
#include <avr/interrupt.h>
//#include <avrlibtypes.h>
//#include <avrlibdefs.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#define STATUS_REG 			SREG
#define Interrupt_Flag		SREG_I
#define Disable_Interrupt	cli();
#define Enable_Interrupt	sei();
#define Is_Interrupt 		(STATUS_REG & (1<<Interrupt_Flag))

#define INLINE __attribute__((always_inline))

typedef int BOOL;
typedef unsigned char uint8_t;

#define TRUE 1
#define FALSE 0

#define RTOS_ISR  			TIMER0_COMPA_vect 

//Clock Config
#define F_CPU 16000000L

//System Timer Config
#define Prescaler	  	64
#define	TimerDivider  		(F_CPU/Prescaler/1000)		// 1 mS

#define baudrate 9600L
#define bauddivider ((F_CPU+baudrate*8)/(baudrate*16)-1)

#define HI(x) ((x)>>8)
#define LO(x) ((x)& 0xFF)

#define TASK_COUNT 10
#define TIMER_COUNT 10

enum STATUS_RTOS
{
	TASK_QUEUE_OK,
	TASK_QUEUE_OVERFLOW
};



#endif  // _ENV_