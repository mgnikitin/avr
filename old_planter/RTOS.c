/*
 * skeleon project
 */ 

#include "config.h"
#include "task_mngr.h"
#include "queuebuf.h"
#include "routing.h"
#include "asmroutine.h"
 #include "unnet.h"
 #include "prot.h"		// протокол обмена
//

#define LED_FAIL STATUS_LED

#define UART_BUFFER_SIZE 32	// use &

// массив байт UART на отправку
static uint8_t arr_uart_out[UART_BUFFER_SIZE];
// массив байт, считанных с UART
static uint8_t arr_uart_in[UART_BUFFER_SIZE];

static volatile t_que q_uart_out;
static volatile t_que q_uart_in;

static uint8_t tmp_arr_out[PROT_BUFFER_SIZE];

#define SwonLED(led) LED_PORT |= _BV(led)	 	/* логичеcкая единица */
#define SwoffLED(led) LED_PORT &= ~_BV(led)		/* логический ноль */
#define IsLED(led) (LED_PORT_RO & _BV(led))

extern void DiodeOffTask(void);
extern void uart_log(uint8_t);
extern void MoistureProbePowerTask(void);


//RTOS Interrupt
ISR(RTOS_ISR) {
	TASK_PTR(TSK_TR_SRV)->active = 1;
}

ISR(TIMER2_COMPA_vect) {
	if(UNET_PORT_RO & _BV(UNET_PIN))
		UNET_REG |= UNET_PIN_MASK;
	else
		UNET_REG &= ~UNET_PIN_MASK;
	inc_sample_cnt_macro
	if(UNET_REG & ST_WRITE_MASK) {
		if(UNET_REG & UNET_WR_PIN_MASK)
			turn_write_macro
		else
			turn_read_macro
	}
	TASK_PTR(TSK_UNET_HANDLE)->active = 1;
}

/* USART Rx Complete */
ISR(USART_RX_vect) {
	// принудительно записываем в буфер,
	// если в буфере нет места. первый элемент
	// будет удален
	q_uart_in.arr[q_uart_in.offset_set] = UDR;
	q_uart_in.offset_set = (q_uart_in.offset_set + 1) & (UART_BUFFER_SIZE - 1);
//	if(q_uart_in.offset_set == q_uart_in.offset_get)
		//q_uart_in.offset_get = (q_uart_in.offset_get + 1) & (UART_BUFFER_SIZE - 1);
}

/* USART, Data Register Empty */
ISR(USART_UDRE_vect) {
	UDR = q_uart_out.arr[q_uart_out.offset_get];
	q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);
	// очередь пуста, запрещаем прерывания по опустошению буфера передачи UDR UART
	if(q_uart_out.offset_get == q_uart_out.offset_set)
		Disable_UART_Int(UDRIE_MASK);
}

/* USART Tx Complete */
ISR(USART_TX_vect) {
	
}

void write_to_uart_buf(const uint8_t *arr, uint8_t sz) {
	uint8_t i;
	for(i = 0; i < sz; i++)
	{
		uart_log(arr[i]);
	}
}

void uart_log(uint8_t b) {
	uint8_t sreg = STATUS_REG;
	Disable_Interrupt
	Disable_UART_Int(UDRIE_MASK);
	STATUS_REG = sreg;
	
	q_uart_out.arr[q_uart_out.offset_set] = b;
	q_uart_out.offset_set = (q_uart_out.offset_set + 1) & (UART_BUFFER_SIZE - 1);
	if(q_uart_out.offset_set == q_uart_out.offset_get)
		q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);
	
	Enable_UART_Int(UDRIE_MASK);
}

void unet_handlr(const t_unet_pckt *ppckt) {

	uint8_t sz;
	char arr_data[2];

	if(ppckt->rcv_crc != ppckt->crc) {
		return;
	}

	hex2ascii(ppckt->dat[0], (char *)arr_data);

	sz = prot_make_RTRDT(PROT_BROADCAST_ADDR, PROT_ADDR, PROT_C_CNT,
		PROT_C_NONE, 2, (uint8_t *)arr_data, &tmp_arr_out[0]);
	write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], sz);

	switch(ppckt->cmd) {
	case C_PING_RQ:			// запрос пинг
	break;
	default:
	break;
	}
}

// ------------------------------------------------------------------
void MoistureGetTask(void) {

	uint8_t sz;
	uint16_t val = ADCH;
	char arr_data[2];
	
	PROBEPWR_PORT &= ~_BV(PROBEPWR_PIN);	// power low	

	hex2ascii(0xFF & val, (char *)arr_data);
	//uart_log(val);
	sz = prot_make_RTRDT(PROT_BROADCAST_ADDR, PROT_ADDR, PROT_C_HMDTY,
		PROT_C_NONE, 2, (uint8_t *)arr_data, &tmp_arr_out[0]);
	write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], sz);
	

	TASK_PTR(TSK_MOISTURE)->hdlr = MoistureProbePowerTask;
	SetTimerTask(TASK_PTR(TSK_MOISTURE), 995);
}

// ------------------------------------------------------------------
void MoistureProbePowerTask(void) {

	PROBEPWR_PORT |= _BV(PROBEPWR_PIN);
	ADCSRA |= _BV(ADSC);	// start ADC
	TASK_PTR(TSK_MOISTURE)->hdlr = MoistureGetTask;
	SetTimerTask(TASK_PTR(TSK_MOISTURE), 5);
}

// ------------------------------------------------------------------
void DiodeOnTask(void) {
	SwonLED(STATUS_LED);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOffTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 10);
}

// ------------------------------------------------------------------
void DiodeOffTask(void) {
	SwoffLED(STATUS_LED);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOnTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}

// ------------------------------------------------------------------
void InitAll(void)
{
	// init buffers
	q_uart_out.arr = arr_uart_out;
	q_uart_out.offset_set = 0;
    q_uart_out.offset_get = 0;
	q_uart_in.arr = arr_uart_in;
	q_uart_in.offset_set = 0;
    q_uart_in.offset_get = 0;
	//InitUSART
	// set baudrate
	UBRRL = LO(bauddivider);
	UBRRH = HI(bauddivider);
	// 
	UCSRA = 0;
	// enable receiver and transmiter
	UCSRB = _BV(RXEN)|_BV(TXEN);
	Disable_UART_Int(TXCIE_MASK|UDRIE_MASK);
	Enable_UART_Int(RXCIE_MASK);
	UCSRC = /*1<<URSEL|*/1<<UCSZ0|1<<UCSZ1;
	// Timer 0
	TCCR0A = 1<<WGM01;				// CTC Mode ( Автосброс после достижения регистра сравнения)
	TCCR0B = TCCR0B_PRESCALER;	    // Prescaler
	TCNT0 = 0;						// Установить начальное значение счётчиков
	OCR0A  = TimerDivider; 			// Установить значение в регистр сравнения
	TIMSK0 |= _BV(OCIE0A);			// Разрешаем прерывание RTOS - запуск ОС
	// Timer 2
	TCCR2A = _BV(WGM21);			// CTC Mode
	TCCR2B = UNET_PRESC;		    // Prescaler
	TCNT2  = 0;
	OCR2A  = UNET_COMP;
	TIMSK2 |= _BV(OCIE2A);
	// led
	LED_DDR |= 1 << STATUS_LED;		// 
	// probe power pin
	PROBEPWR_PORT &= ~_BV(PROBEPWR_PIN);
	PROBEPWR_DDR |= _BV(PROBEPWR_PIN);
	// ADC
	ADMUX = 0;
	ADMUX |= _BV(REFS0) | _BV(ADLAR) | _BV(MUX2) | _BV(MUX0);
	ADCSRA = /*_BV(ADPS2) | */_BV(ADPS1) | _BV(ADPS0);	// prescaler
	ADCSRA |= _BV(ADEN) /*| _BV(ADSC)*/;	// включаем АЦП, запускаем преобразование
	// UnNet task
	if(!unet_init(unet_handlr))
		SwonLED(LED_FAIL);
}

// ------------------------------------------------------------------
int main(void)
{
	InitAll();
	InitRTOS();
	tm_reg_task(TSK_DIOD, DiodeOnTask);
	tm_reg_task(TSK_MOISTURE, MoistureProbePowerTask);
	tm_reg_task(TSK_UNET_DEQUEUE, unet_deque_task);
	tm_reg_task(TSK_UNET_RECEIVE, receive_func);
	tm_reg_task(TSK_UNET_HANDLE, unet_timer_handle);
	//
	// Diod task
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
	//
	//SetTimerTask(TASK_PTR(TSK_MOISTURE), 1000);

	Enable_Interrupt				// разрешаем прерывания
	tm_exec();
	return 0;
}