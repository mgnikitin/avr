
#define UNET_ADDR 0x07
#define PROT_ADDR UNET_ADDR
// Port B
// Port C
#define UNET_PORT 	PORTC
#define UNET_PORT_RO PINC
#define UNET_DDR 	DDRC
#define UNET_PIN 	1
#define PROBEPWR_DDR DDRC
#define PROBEPWR_PORT PORTC
#define PROBEPWR_PIN 4
// Port D
#define LED_DDR 	DDRD
#define LED_PORT 	PORTD
#define LED_PORT_RO PIND
#define STATUS_LED 	7

// tasks id
// 0 prohibited
#define TSK_UNET_HANDLE  1
#define TSK_UNET_DEQUEUE 2
#define TSK_UNET_RECEIVE 3
#define TSK_DIOD  		 4
#define TSK_MOISTURE	 5

//
//#define swon_unet_led LED_PORT |= _BV(STATUS_LED)

//#define swoff_unet_led LED_PORT &= ~_BV(STATUS_LED)
