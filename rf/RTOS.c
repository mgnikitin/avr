/*
 * RTOS.c
 *
 * Created: 11.03.2014 22:11:35
 *  Author: Asus
 */ 

#include "env.h"
#include "queuebuf.h"
#include "routing.h"
#include "asmroutine.h"
#include "virtual_wire.h"
// Port B

// Port C
#define LED_DDR DDRC
#define LED_PORT PORTC
#define LED_PORT_RO PINC
#define STATUS_LED 5
#define UNET_PORT PORTC
#define UNET_PORT_RO PINC
#define UNET_DDR DDRC
#define UNET_PIN 0
// Port D
#define SWITCH_DDR DDRD
#define SWITCH_PORT PORTD
#define SWITCH_PIN 3
#define RF_RCV_DDR DDRD
#define RF_RCV_PORT PORTD
#define RF_RCV_PORT_RO PIND
#define RF_RCV_PIN (uint8_t)2
//

#define LED_FAIL STATUS_LED
#define UART_BUFFER_SIZE 20
#define UNET_INTERVAL_MS 1
#define UNET_ADDR 0x05

#define SwonLED(led) LED_PORT |= (1 << led)	 	/* логичеcкая единица  */
#define SwoffLED(led) LED_PORT &= ~(1 << led)	/* логический ноль */
#define IsLED(led) (LED_PORT_RO & (1 << led))


// массив байт UART на отправку
static uint8_t arr_uart_out[UART_BUFFER_SIZE];

volatile t_queue q_uart_out;
//static volatile t_queue q_uart_in;

static volatile uint8_t b_uart_out;

volatile uint16_t t0_compa_cnt = 0;
volatile uint16_t t1_compa_cnt = 0;

volatile uint32_t mm[2] = {0, 0};
volatile uint32_t d;
volatile uint16_t t;
volatile uint8_t  t0_compa_upd = 0;
volatile uint16_t t1_compa_upd = 0;
volatile uint8_t  int0_upd = 0;

#define uart_log_isr_macro(b) 		\
	if(enque(&q_uart_out, b))			\
			Enable_UART_Int(UDRIE_MASK);

// Прерывание таймера 1
//ISR(TIMER1_COMPA_vect) {
	//t1_compa_upd = 1;
//}

/* USART Rx Complete */
/*ISR(USART_RX_vect) {
	// принудительно записываем в буфер,
	// если в буфере нет места. первый элемент
	// будет удален
	enque_f(&q_uart_in, UDR);
}*/

/* USART, Data Register Empty */
ISR(USART_UDRE_vect) {
	if(deque(&q_uart_out, &b_uart_out))
		UDR = b_uart_out;
	else	// очередь пуста, запрещаем прерывания по опустошению буфера передачи UDR UART
		Disable_UART_Int(UDRIE_MASK);
}

/* USART Tx Complete */
ISR(USART_TX_vect) {
	
}

// ------------------------------------------------------------------
void uart_log(uint8_t b) {
	safe_Disable_Interrupt

	if(enque(&q_uart_out, b))
	{
		Enable_UART_Int(UDRIE_MASK);
	}
	safe_Enable_Interrupt
}

// ------------------------------------------------------------------
inline INLINE void InitAll(void)
{
	// init buffers
	init_que(&q_uart_out, arr_uart_out, UART_BUFFER_SIZE);
	//init_que(&q_uart_in, arr_uart_in, UART_BUFFER_SIZE);
	//InitUSART
	// set baudrate
	UBRRL = LO(bauddivider);
	UBRRH = HI(bauddivider);
	// 
	UCSRA = 0;
	// enable receiver and transmiter
	UCSRB = 1<<RXEN|1<<TXEN;
	Disable_UART_Int(TXCIE_MASK|UDRIE_MASK);
//	Enable_UART_Int(RXCIE_MASK);
	UCSRC = /*1<<URSEL|*/1<<UCSZ0|1<<UCSZ1;
	// Timer 0
	//TCCR0A = 1<<WGM01;				// CTC Mode - Автосброс после достижения регистра сравнения
	//TCCR0B = (0<<CS00) | (1<<CS01);				// Prescaler: Freq = CK/8 -
	//TCNT0 = 0;						// Установить начальное значение счётчиков
	//OCR0A  = F_CPU / 8L / 10000L; 		// Установить значение в регистр сравнения
	//TIMSK0 = 1<<OCIE0A;			// Разрешаем прерывание RTOS - запуск ОС
	// Timer 1
	//TCCR1A = 0;
	//TCCR1B = 1 << WGM12;		// CTC Mode
	//TCCR1B |= (1 << CS10);		// Prescaler:  no
	//OCR1A =  (F_CPU/ 1000L);		// write 16-bit register
	//TIMSK1 = 1<<OCIE1A;
	// external interrupt
	//EICRA = (0 << ISC01) | (1 << ISC00);
	//EIMSK = (1 << INT0);
	//RF_RCV_DDR &= ~(1 << RF_RCV_PIN);
	//RF_RCV_PORT |= 1 << RF_RCV_PIN;
	// led
	//LED_DDR |= 1 << STATUS_LED;		// 
	//
	//SWITCH_DDR |= 1 << SWITCH_PIN;
}


int main(void)
{
	uint8_t buf[VW_MAX_MESSAGE_LEN];
    uint8_t buflen = VW_MAX_MESSAGE_LEN;
	// Initialise the IO and ISR   
    vw_setup(2000);      // Bits per sec

    vw_rx_start();       // Start the receiver PLL running
	InitAll();
	Enable_Interrupt
	
	uart_log('A');
	
	while(1)
	{
		if (vw_get_message(buf, &buflen)) // Non-blocking
		{
			int i;
		// digitalWrite(2, true); // Flash a light to show received good message
			// Message with a good checksum received, dump it.
			//Serial.print("Got: ");       
			for (i = 0; i < buflen; i++)
			{
				uart_log((char)buf[i]);
				uart_log(' ');
			}
			uart_log('\n');
		//  digitalWrite(2, false);
		}
	}
	return 0;
}