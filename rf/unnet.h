/**
  * version 0.4
  * 14.03.2015
  */

#include "env.h"
#include "queuebuf.h"

typedef enum
{
	ST_IDLE,			///< \brief линия свободна
	ST_INIT_READING,	///< \brief инициализация чтения
	ST_SYNC_READING,	///< \brief синхронизация чтения бита
	ST_READING,			///< \brief чтение бита
	ST_INIT_SENDING,	///< \brief инициализация записи
	ST_SEND_SYNC,		///< \brief синхронизация записи бита
	ST_SEND_ONE_BIT,	///< \brief запись логической 1
	ST_SEND_ZERO_BIT	///< \brief запись логического 0
} DEVICE_STATE;

typedef enum
{
	C_NONE = 0x00,
	C_PING_RQ = 0x01,		// запрос наличия устройства
	C_PING_RP = 0x02,		// ответ на команду C_PING_RQ
	C_ACCPT = 0x03,			// ответ на команду (сигнализировать о том, что команда принята)
	C_SWON_L = 0x04,		// включить освещение
	C_SWOFF_L = 0x05,		// выключить освещения
	C_ALM_W = 0x10,			// датчик протечек тревога
	C_DATA = 0xF0			// данные
} COMMAND_TYPE;

typedef struct {
	uint8_t addr_dst;		///< \brief адрес назначения
	uint8_t addr_src;		///< \brief адрес источника (отправителя)
	uint8_t cmd;			///< \brief тип команды
	uint8_t sz;				///< \brief размер поля Дата (для команды C_DATA)
	uint8_t *data;			///< \brief указатель на массив данных (поле Дата, команда C_DATA)
	
} t_unet_pckt;

#define UNET_BROADCAST_ADDR 0xFF	// широковещательный адрес

#define DATA_FLD_SZ 10				// размер поля Дата пакета


BOOL unet_init(	uint8_t pn, 
				volatile uint8_t *prt, 
				volatile uint8_t *prt_ro,
				volatile uint8_t *prt_dir, 
				long period_mks,
				uint8_t addr,	// собственный адрес
				void (*recv_hanldr)(const t_unet_pckt *));

void unet_timer_handle(void);

extern BOOL unet_sendb(uint8_t addr, uint8_t b);

// отправить сообщение
extern BOOL unet_send(uint8_t addr, uint8_t *arr, uint8_t sz);

// поместить сообщение в стек
extern BOOL unet_enque(const t_unet_pckt *ppckt);