/**
  * version 0.4
  * 14.03.2015
  */
#include "unnet.h"
#include <string.h>

extern void uart_log(uint8_t);	// TODO

typedef enum {
	RV_NONE,
	RV_ADDR_DEST,		///< \brief прием адреса назначения
	RV_ADDR_SRC,		///< \brief прием адреса отправителя
	RV_CMD,				///< \brief прием команды
	RV_SIZE,			///< \brief прием размера поля данных
	RV_DATA,			///< \brief прием данных
	RV_FINISHED
} RECEIVE_STATE;

#define T_INTERVAL_MIN_MKS 100L
// размер буфера на прием
#define BUFFER_SZ 20
// размер стека буфера отложенных пакетов для передачи
#define UNET_PCKT_STACK_SZ 10

// минимальный временной интервал инициализации начала передачи
static const long INIT_READING_MIN_MKS = (10L * T_INTERVAL_MIN_MKS);
static const long INIT_READING_MAX_MKS = (20L * T_INTERVAL_MIN_MKS);

// максимальный временной интервал синхронизирующего импульса
static const long SYNC_DELAY_MAX_MKS = (2L * T_INTERVAL_MIN_MKS);

static const long LOGIC_ZERO_MIN_MKS = T_INTERVAL_MIN_MKS;
static const long LOGIC_ONE_MIN_MKS = (3L * T_INTERVAL_MIN_MKS);

static DEVICE_STATE curr_state = ST_IDLE;

static int dt_mks = 1;	//mks
static long counter = 0;

static uint8_t bit_shift = 0;				// смещение бита не чтение (запись)
static uint8_t databyte = 0;
static RECEIVE_STATE rcv_st = RV_NONE; 		// состояние приема (чему соответствует принимаемый байт)
static uint8_t arr_bufo[BUFFER_SZ];			// буфер на отправку
static uint8_t num_bufo = 0;				// номер отправляемого байта
static uint8_t sz_bufo = 0;					// размер буфера на отправку
static uint8_t sz_data_in = 0;
static uint8_t arr_data_in[DATA_FLD_SZ];	// массив поля Дата пакета на прием
static t_unet_pckt unet_pckt_rv = { 0, 0, 0, 0, (uint8_t *)&arr_data_in};	// пакет на прием

// массив пакетов unet, хранящий неотправленные пакеты
static t_unet_pckt arr_unet_pckt[UNET_PCKT_STACK_SZ];
// массив полей Дата пакета unet
static uint8_t arr_unet_pckt_data[UNET_PCKT_STACK_SZ][DATA_FLD_SZ];
// массив смещений адресов неотправленных пакетов unet
static uint8_t arr_unet_pckt_addr_sh[UNET_PCKT_STACK_SZ];
// очередь пакетов unet на передачу (содержит адреса пакетов в памяти)
static t_queue q_unet_pckt_addr;
static uint8_t b = 0;

static uint8_t own_addr = 0x01;		// собственный адрес
volatile uint8_t *pport = &PORTB;	// GPIO-порт
volatile uint8_t *pport_ro = &PINB;	// GPIO-порт на чтение
volatile uint8_t *pddr = &DDRB;		// направление GPIO-порта
static uint8_t pin;					// бит GPIO-порта

//#define SEND_HIGH() *pport |= (1 << pin)
#define SEND_LOW()  *pport &= ~(1 << pin)
#define IS_HIGH() (*pport_ro & (1<<pin))
#define IS_LOW() (!(*pport_ro & (1<<pin)))

static void empty_func(const t_unet_pckt * ppckt) {
}

void (*recv_func)(const t_unet_pckt *ppckt) = empty_func;

inline INLINE void parse_byte(void)
{
	if(RV_ADDR_DEST == rcv_st) {		// состояние приема адреса назначения
		unet_pckt_rv.addr_dst = databyte;
		rcv_st = RV_ADDR_SRC;
	}
	else if(RV_ADDR_SRC == rcv_st) {	// состояние приема адреса отправителя
		unet_pckt_rv.addr_src = databyte;
		rcv_st = RV_CMD;
	}
	else if(RV_CMD == rcv_st) {
		unet_pckt_rv.cmd = databyte;
		if(C_DATA == databyte)
		{
			rcv_st = RV_SIZE;
		}
		else
		{
			rcv_st = RV_FINISHED;
		}
	}
	else if(RV_SIZE == rcv_st) {
		unet_pckt_rv.sz = 0;
		sz_data_in = databyte;
		// ограничение по размеру поля Дата пакета
		rcv_st = ((sz_data_in == 0) || (sz_data_in > DATA_FLD_SZ)) ? RV_NONE : RV_DATA;
	}
	else if(RV_DATA == rcv_st) {
		unet_pckt_rv.data[unet_pckt_rv.sz] = databyte;
		unet_pckt_rv.sz++;
		if(unet_pckt_rv.sz == sz_data_in)
		{
			rcv_st = RV_FINISHED;
		}
		else
			rcv_st = RV_DATA;
	}
	else if(RV_FINISHED == rcv_st) {
		rcv_st = RV_NONE;
	}
	else if(RV_NONE == rcv_st) {
	}
	else {
		// undefined
	}
}

// переходим к чтению
inline INLINE void turn_read(void) {
	if(*pddr & (1 << pin))
		*pddr &= ~(1 << pin);
}

// переходим к записи
inline INLINE void turn_write(void) {
	if(~(*pddr) & (1 << pin))
		*pddr |= (1 << pin);
}

BOOL unet_init(	uint8_t pn, 
				volatile uint8_t *prt, 
				volatile uint8_t *prt_ro, 
				volatile uint8_t *prt_dir, 
				long period_mks,
				uint8_t addr,
				void (*recv_hanldr)(const t_unet_pckt *)
				)
{
	uint8_t i = 0;
	pin = pn;
	pport = prt;
	pport_ro = prt_ro;
	pddr = prt_dir;
	dt_mks = period_mks;
	own_addr = addr;
	recv_func = recv_hanldr;
	
	init_que(&q_unet_pckt_addr, arr_unet_pckt_addr_sh, UNET_PCKT_STACK_SZ);
	for(i = 0; i < UNET_PCKT_STACK_SZ; i++)
	{
		arr_unet_pckt[i].data = arr_unet_pckt_data[i];
		arr_unet_pckt_addr_sh[i] = i;
	}
	
	if(dt_mks > T_INTERVAL_MIN_MKS)
		return FALSE;
	// устанавливаем направление порта на чтение
	*pddr &= ~(1 << pn);	// input
	SEND_LOW();
	//
	return TRUE;
}

void unet_timer_handle(void)
{	
	if(ST_IDLE == curr_state)
	{
		turn_read(); 		// меняем напрвление порта на чтение 
		//
		if(IS_LOW())
		{	// обнаружен импульс сброса
			counter = 0;
			if(sz_bufo != 0)
			{
				// TODO
				uart_log('x');
			}
			curr_state = ST_INIT_READING;
			bit_shift = 0;			// сбрасываем бит смещения
			
		}
		else if(sz_bufo != 0)		// high
		{	// есть неотправленные байты
			counter = 0;
			curr_state = ST_INIT_SENDING;
			turn_write();		// меняем напрвление порта на запись (производим инициализацию передачи)
			bit_shift = 0;			// сбрасываем бит смещения
		}
		else if(deque(&q_unet_pckt_addr, &b))
		{	// есть неотправленные пакеты
			sz_bufo = 3;
			//uart_log('d');
			arr_bufo[0] = arr_unet_pckt[b].addr_dst;
			arr_bufo[1] = arr_unet_pckt[b].addr_src;
			arr_bufo[2] = arr_unet_pckt[b].cmd;
			if(C_DATA == arr_unet_pckt[b].cmd)
			{
				sz_bufo += arr_unet_pckt[b].sz;
				arr_bufo[3] = arr_unet_pckt[b].sz;
				memcpy(&arr_bufo[3], arr_unet_pckt[b].data, arr_unet_pckt[b].sz);
			}
			num_bufo = 0;
			counter = 0;
			curr_state = ST_INIT_SENDING;
			turn_write();		// меняем напрвление порта на запись (производим инициализацию передачи)
			bit_shift = 0;			// сбрасываем бит смещения
		}
	}
	else if(ST_INIT_READING == curr_state)		// состояние инициализации чтения
	{
		counter += dt_mks;
		if(IS_HIGH())
		{
			// если временной интервал импульса сброса
			// соответствует интервалу инициализации, то
			// переходим к чтению
			if(counter >= INIT_READING_MIN_MKS)
			{
				counter = 0;
				rcv_st = RV_ADDR_DEST;			// состояние приема на чтение адреса назначения
				curr_state = ST_SYNC_READING;
				databyte = 0;
			}
			else
			{	// слишком короткий импульс сброса для инициализации
				// передачи
				counter = 0;
				curr_state = ST_IDLE;
			}
		}
		else
		{
			if(counter > INIT_READING_MAX_MKS)
			{
				// слишком длинный импульс сброса для инициализации
				// передачи
				counter = 0;
				curr_state = ST_IDLE;
			}
		}
	}
	else if(ST_SYNC_READING == curr_state)		// синхронизация чтения бита
	{
		counter += dt_mks;
		if(IS_HIGH())
		{  // ждем окончания синхронизирующего импульса
			if(counter > SYNC_DELAY_MAX_MKS)
			{
				// слишком длинный синхронизирующий импульс
				counter = 0;
				curr_state = ST_IDLE;
			}
		}
		else
		{	// переходим к чтению
			counter = 0;
			curr_state = ST_READING;
		}
		
	}
	else if(ST_READING == curr_state)			// чтение бита
	{
		counter += dt_mks;
		if(IS_HIGH())
		{	// обнаружен синхронизирующий импульс
			if(counter >= LOGIC_ONE_MIN_MKS)
			{	// обнаружена 1
				databyte |= 1 << (7-bit_shift);
				bit_shift++;
				if(bit_shift > 7)
				{
					bit_shift = 0;
					parse_byte();
					databyte = 0;		// обнуляем байт чтения
				}
			}
			else if(counter >= LOGIC_ZERO_MIN_MKS)
			{	// обнаружен 0
				databyte &= ~(0 << (7-bit_shift));
				bit_shift++;
				if(bit_shift > 7)
				{
					bit_shift = 0;
					parse_byte();
					databyte = 0;		// обнуляем байт чтения
				}
			}
			else
			{
				// слишком короткий интервал времени для регистрации
				// логического уровня сигнала TODO
				counter = 0;
				curr_state = ST_IDLE;
			}
			counter = 0;
			// если больше не ждем поступления байта, переходим к состоянию ожидания инициализации
			if(RV_FINISHED == rcv_st)
			{
				curr_state = ST_IDLE;
				// вызываем обработчик пакета, если адрес назначения совпал 
				// с собственным или пакет передан по широковещательному адресу
				if((own_addr == unet_pckt_rv.addr_dst) 
						|| (UNET_BROADCAST_ADDR == unet_pckt_rv.addr_dst))
					recv_func((const t_unet_pckt *)&unet_pckt_rv);
				rcv_st = RV_NONE;
			}
			else
				curr_state = ST_SYNC_READING;
		}
		else
		{
			if(counter >= INIT_READING_MIN_MKS)
			{	// обнаружен сброс (инициализация)
				counter = 0;
				curr_state = ST_INIT_READING;
			}
		}	
	}
	else if(ST_INIT_SENDING == curr_state)		// состояние инициализации отправки
	{	// инициализация передачи
		counter += dt_mks;
		if(counter > INIT_READING_MIN_MKS)
		{
			counter = 0;
			turn_read();
			curr_state = ST_SEND_SYNC;
		}
	}
	else if(ST_SEND_SYNC == curr_state)			// состояние синхронизации записи бита
	{	// синхронизирующий импульс передачи
		counter += dt_mks;
		if(counter > T_INTERVAL_MIN_MKS)
		{
			counter = 0;
			turn_write();
			curr_state = (arr_bufo[num_bufo] & (1<<(7-bit_shift))) ? ST_SEND_ONE_BIT : ST_SEND_ZERO_BIT;
		}
	//	else if(IS_LOW())
	//	{// обнаружен импульс сброса, линия занята
	//		curr_state = ST_IDLE;
	//	}
	}
	else if(ST_SEND_ONE_BIT == curr_state)		// состояние записи логической 1
	{
		counter += dt_mks;
		if(counter > LOGIC_ONE_MIN_MKS)
		{
			counter = 0;
			//uart_log('1');
			turn_read();
			bit_shift++;
			if(bit_shift > 7)		// байт отправлен
			{
				num_bufo++;			// переходим к следующему байту
				if(num_bufo != sz_bufo) {	// еще не все отправили 
					bit_shift = 0;
					curr_state = ST_SEND_SYNC;
				}
				else {
					sz_bufo = 0;		// сбрасываем буфер
					curr_state = ST_IDLE;
				}
			}
			else {
				curr_state = ST_SEND_SYNC;
			}
		}
	}
	else if(ST_SEND_ZERO_BIT == curr_state)		// состояние записи логического 0
	{
		counter += dt_mks;
		if(counter > LOGIC_ZERO_MIN_MKS)
		{
			counter = 0;
			//uart_log('0');
			turn_read();
			bit_shift++;
			if(bit_shift > 7)		// байт отправлен
			{
				num_bufo++;			// переходим к следующему байту
				if(num_bufo != sz_bufo)	{	// еще не все отправили
					bit_shift = 0;
					curr_state = ST_SEND_SYNC;
				}
				else {
					sz_bufo = 0;		// сбрасываем буфер
					curr_state = ST_IDLE;
				}
			}
			else {
				curr_state = ST_SEND_SYNC;
			}
		}
	}
}

BOOL unet_sendb(uint8_t addr, uint8_t b)
{
	return unet_send(addr, &b, 1);
}

// отправить сообщение
BOOL unet_send(uint8_t addr, uint8_t *arr, uint8_t sz)
{
	if(curr_state != ST_IDLE)
		return FALSE;		// линия занята
	if((sz > (BUFFER_SZ - 2)) || (sz_bufo != 0))
		return FALSE;
	else
	{
		sz_bufo = sz + 2;
		arr_bufo[0] = addr;
		arr_bufo[1] = own_addr;
		num_bufo = 2;
		while(sz > 0)
		{
			arr_bufo[num_bufo] = arr[num_bufo-2];
			sz--;
			num_bufo++;
		}
		num_bufo = 0;
		return TRUE;
	}
}

// поместить сообщение в стек
BOOL unet_enque(const t_unet_pckt *ppckt)
{
	uint8_t b;
	if(acque(&q_unet_pckt_addr, &b))
	{
		arr_unet_pckt[b].addr_dst = ppckt->addr_dst;
		arr_unet_pckt[b].addr_src = ppckt->addr_src;
		arr_unet_pckt[b].cmd = ppckt->cmd;
		if(C_DATA == ppckt->cmd)
		{
			arr_unet_pckt[b].sz = ppckt->sz;
			memcpy(arr_unet_pckt[b].data, ppckt->data, ppckt->sz);
		}
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}