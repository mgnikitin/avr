/*
 * skeleon project
 */ 

#include "config.h"
#include "task_mngr.h"
#include "queuebuf.h"
#include "routing.h"
#include "asmroutine.h"
 #include "unnet.h"
 #include "prot.h"		// протокол обмена
//

 #undef RTOS_ISR
#define RTOS_ISR  			TIMER2_COMPA_vect 

#define LED_FAIL STATUS_LED

#define UART_BUFFER_SIZE 32	// use &

// массив байт UART на отправку
static uint8_t arr_uart_out[UART_BUFFER_SIZE];
// массив байт, считанных с UART
static uint8_t arr_uart_in[UART_BUFFER_SIZE];

static volatile t_que q_uart_out;
static volatile t_que q_uart_in;

#define SENT_SIZE 2
static t_sent sents[SENT_SIZE];
static uint8_t tmp_arr_out[PROT_BUFFER_SIZE];

#define SwonLED(led) LED_PORT |= _BV(led)	 	/* логичеcкая единица */
#define SwoffLED(led) LED_PORT &= ~_BV(led)		/* логический ноль */
#define IsLED(led) (LED_PORT_RO & _BV(led))

extern void DiodeOffTask(void);
extern void uart_log(uint8_t);
extern void protCollectTask(void);
extern void MoistureProbePowerTask(void);

static uint16_t power_cntdown = 10000;	// таймер работы мотора
static BOOL is_measuring = FALSE;
static BOOL use_autowatering = FALSE;  // использовать автополив
static uint16_t autowatering_cntup_sec = 0;	// интервал времени с момента последнего автополива


//RTOS Interrupt
ISR(RTOS_ISR) {
	TASK_PTR(TSK_TR_SRV)->active = 1;
}

/*
ISR(TIMER2_COMPA_vect) {
	if(UNET_PORT_RO & _BV(UNET_PIN))
		UNET_REG |= UNET_PIN_MASK;
	else
		UNET_REG &= ~UNET_PIN_MASK;
	inc_sample_cnt_macro
	if(UNET_REG & ST_WRITE_MASK) {
		if(UNET_REG & UNET_WR_PIN_MASK)
			turn_write_macro
		else
			turn_read_macro
	}
	TASK_PTR(TSK_UNET_HANDLE)->active = 1;
}
*/

/* USART Rx Complete */
ISR(USART_RX_vect) {
	// принудительно записываем в буфер,
	// если в буфере нет места. первый элемент
	// будет удален
	q_uart_in.arr[q_uart_in.offset_set] = UDR;
	q_uart_in.offset_set = (q_uart_in.offset_set + 1) & (UART_BUFFER_SIZE - 1);
//	if(q_uart_in.offset_set == q_uart_in.offset_get)
		//q_uart_in.offset_get = (q_uart_in.offset_get + 1) & (UART_BUFFER_SIZE - 1);
}

/* USART, Data Register Empty */
ISR(USART_UDRE_vect) {
	UDR = q_uart_out.arr[q_uart_out.offset_get];
	q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);
	// очередь пуста, запрещаем прерывания по опустошению буфера передачи UDR UART
	if(q_uart_out.offset_get == q_uart_out.offset_set)
		Disable_UART_Int(UDRIE_MASK);
}

/* USART Tx Complete */
ISR(USART_TX_vect) {
	
}

// ------------------------------------------------------------------
void set_pwm(uint8_t b) {
	OCR0A = b;
}

// ------------------------------------------------------------------
void set_stop(void) {

	IN1_PORT &= ~_BV(IN1_PIN);
	IN2_PORT &= ~_BV(IN2_PIN);
}

// ------------------------------------------------------------------
BOOL set_up(void) {
	
	// задаем вращение мотора только, если он не активен
	if(0 == power_cntdown) {
		power_cntdown = 10000;
		IN2_PORT &= ~_BV(IN2_PIN);
		IN1_PORT |= _BV(IN1_PIN);		
		autowatering_cntup_sec = 0;
		return TRUE;
	}
	return FALSE;
}

// ------------------------------------------------------------------
BOOL set_down(void) {
	
	// задаем вращение мотора только, если он не активен
	if(0 == power_cntdown) {
		power_cntdown = 10000;
		IN1_PORT &= ~_BV(IN1_PIN);
		IN2_PORT |= _BV(IN2_PIN);
		autowatering_cntup_sec = 0;
		return TRUE;
	}
	return FALSE;
}

// ------------------------------------------------------------------
void write_to_uart_buf(const uint8_t *arr, uint8_t sz) {
	uint8_t i;
	for(i = 0; i < sz; i++)
	{
		uart_log(arr[i]);
	}
}

// ------------------------------------------------------------------
void uart_log(uint8_t b) {
	uint8_t sreg = STATUS_REG;
	Disable_Interrupt
	Disable_UART_Int(UDRIE_MASK);
	STATUS_REG = sreg;
	
	q_uart_out.arr[q_uart_out.offset_set] = b;
	q_uart_out.offset_set = (q_uart_out.offset_set + 1) & (UART_BUFFER_SIZE - 1);
	if(q_uart_out.offset_set == q_uart_out.offset_get)
		q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);
	
	Enable_UART_Int(UDRIE_MASK);
}

// ------------------------------------------------------------------
void unet_handlr(const t_unet_pckt *ppckt) {

	uint8_t sz;
	char arr_data[2];

	if(ppckt->rcv_crc != ppckt->crc) {
		return;
	}

	hex2ascii(ppckt->dat[0], (char *)arr_data);

	sz = prot_make_RTRDT(PROT_BROADCAST_ADDR, PROT_ADDR, PROT_C_CNT,
		PROT_C_NONE, 2, (uint8_t *)arr_data, &tmp_arr_out[0]);
	write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], sz);

	switch(ppckt->cmd) {
	case C_PING_RQ:			// запрос пинг
	break;
	default:
	break;
	}
}

// ------------------------------------------------------------------
void Service1HzTask(void) {

	if(autowatering_cntup_sec < 0xffffUL) {
		autowatering_cntup_sec++;
	}
}

// ------------------------------------------------------------------
void MoistureGetTask(void) {

	if(ADCSRA & _BV(ADSC)) {
		// conversion is in progress
		SetTimerTask(TASK_PTR(TSK_MOISTURE), 1);
	}
	else {
		uint8_t sz;
		uint16_t val = ADCH;
		char arr_data[2];
	
		// discharge capacitor
		PROBEPWR_PORT &= ~_BV(PROBEPWR_PIN);	// power low	
		DISCHARGE_DDR |= _BV(DISCHARGE_PIN);	// discharge output
		DISCHARGE_PORT &= ~_BV(DISCHARGE_PIN);	// low

		hex2ascii(0xFF & val, (char *)arr_data);
		//uart_log(val);
		sz = prot_make_RTRDT(PROT_BROADCAST_ADDR, PROT_ADDR, PROT_C_HMDTY,
			PROT_C_NONE, 2, (uint8_t *)arr_data, &tmp_arr_out[0]);
		write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], sz);

		if(use_autowatering && (val > 150) && (autowatering_cntup_sec >= 36000UL)) {
			// do autowatering
			set_up();
		}
		

		TASK_PTR(TSK_MOISTURE)->hdlr = MoistureProbePowerTask;
		SetTimerTask(TASK_PTR(TSK_MOISTURE), 1000);
	}
}

// ------------------------------------------------------------------
void MoistureStartMeasureTask(void) {

	ADCSRA |= _BV(ADSC);	// start ADC

	TASK_PTR(TSK_MOISTURE)->hdlr = MoistureGetTask;
	SetTimerTask(TASK_PTR(TSK_MOISTURE), 1);
}

// ------------------------------------------------------------------
void MoistureProbePowerTask(void) {

	DISCHARGE_DDR &= ~_BV(DISCHARGE_PIN);	// discharge input
	PROBEPWR_PORT |= _BV(PROBEPWR_PIN);		// start capacitor charging
	
	TASK_PTR(TSK_MOISTURE)->hdlr = MoistureStartMeasureTask;
	SetTimerTask(TASK_PTR(TSK_MOISTURE), 100);

	is_measuring = TRUE;
}

// ------------------------------------------------------------------
void DiodeOnTask(void) {
	SwonLED(STATUS_LED);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOffTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}

// ------------------------------------------------------------------
void DiodeOffTask(void) {
	SwoffLED(STATUS_LED);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOnTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}

// ------------------------------------------------------------------
// задача пользовательской обработки предложения протокола обмена
void protParseItem(void) {
	prot_item();
	// меняем обработчик, накапливаем данные для разбора
	TASK_PTR(TSK_PROT)->hdlr = protCollectTask;
	// отложенный вызов задачи
	SetTimerTask(TASK_PTR(TSK_PROT), 10);
}

// ------------------------------------------------------------------
// задача разбора предложения
void protParseSentTask(void) {
	if(parse_sentence()) {
		// меняем обработчик, вызываем установленный обработчик предложения
		TASK_PTR(TSK_PROT)->hdlr = protParseItem;
	}
	else {
		// меняем обработчик, продолжаем накапливать данные 
		TASK_PTR(TSK_PROT)->hdlr = protCollectTask;
	}
	SetTask(TASK_PTR(TSK_PROT));	// добавляем задачу на выполнение 
}

// ------------------------------------------------------------------
// задача поиска предложений во входном буфере по протоколу обмена
void protParseTask(void) {
	if(prot_parse()) {
		// меняем обработчик на разбор предложения
		TASK_PTR(TSK_PROT)->hdlr = protParseSentTask;
	}
	else {
		// меняем обработчик, продолжаем накапливать данные
		TASK_PTR(TSK_PROT)->hdlr = protCollectTask;
	}
	SetTask(TASK_PTR(TSK_PROT));	// добавляем задачу на выполнение 
}

// ------------------------------------------------------------------
// задача накопления входных данных по протоколу обмена
void protCollectTask(void) {
	uint8_t sreg;
	uint8_t count = 0;
	while(TRUE) {
		if((count < UART_BUFFER_SIZE) && (q_uart_in.offset_set != q_uart_in.offset_get)) {
			sreg = STATUS_REG;
			Disable_Interrupt
			q_prot_in.arr[q_prot_in.offset_set] = q_uart_in.arr[q_uart_in.offset_get];
			q_uart_in.offset_get = (q_uart_in.offset_get + 1) & (UART_BUFFER_SIZE - 1);
			STATUS_REG = sreg;
			q_prot_in.offset_set = (q_prot_in.offset_set + 1) % PROT_BUFFER_SIZE;
			++count;
			if(q_prot_in.offset_get == q_prot_in.offset_set) {
				break;
			}
		}
		else {
			break;
		}
	}
	if(count != 0) {	// проверка поступления данных
		//uart_log(count);
		TASK_PTR(TSK_PROT)->hdlr = protParseTask;	// меняем обработчик
		SetTask(TASK_PTR(TSK_PROT));				// добавляем задачу на выполнение
	}
	else
		SetTimerTask(TASK_PTR(TSK_PROT), 10);
}

// ------------------------------------------------------------------
void ServiceTask(void) {

	static int sec_cntdown = 1000;
	if(sec_cntdown <= 0) {
		Service1HzTask();
		sec_cntdown = 1000;
	}
	else {
		sec_cntdown--;
	}

	if(power_cntdown > 0) {
		power_cntdown--;
		if(0 == power_cntdown) {
			set_stop();	// останавливаем мотор
		}
	}

	//if(is_measuring) {
	//	uint16_t val = ADCH;
	//	
	//	is_measuring = FALSE;
	//}


	SetTimerTask(TASK_PTR(TSK_SRV), 1);
}

// ------ Parsing sentences -----------------------------------------
BOOL parse_CNSMD(const uint8_t *arr, size_t sz)
{
	uint8_t b;
	uint8_t addr_rcv;
	uint8_t addr_frm;
	uint8_t cmd;

	/* $PCNSMD,02,01,0F*21\r\n
     *  |   |  |  |  |  | |
	 *  |   |  |  |  |  | +- end sentence 
     *  |   |  |  |  |  +--- checksum
     *  |   |  |  |  +------ command type
     *  |   |  |  +--------- address of sender
     *  |   |  +------------ address of receiver
     *  |   +--------------- type of sentence (header part)
     *  +------------------- id of sender (header part)
     */
	if(sz != 21)
        return FALSE;
	
	addr_rcv = (ascii2hex(arr[8]) << 4) & 0xF0;
	addr_rcv |= ascii2hex(arr[9]) & 0x0F;
	
	addr_frm = (ascii2hex(arr[11]) << 4) & 0xF0;
	addr_frm |= ascii2hex(arr[12]) & 0x0F;
	
	cmd = (ascii2hex(arr[14]) << 4) & 0xF0;
	cmd |= ascii2hex(arr[15]) & 0x0F;
	
	// обработка команд предназначенных собственному устройству
	if((PROT_ADDR == addr_rcv) || (PROT_BROADCAST_ADDR == addr_rcv)) {

		switch(cmd) {
			case PROT_C_PING_RQ:
				b = prot_make_RTRMD(addr_frm, PROT_ADDR, &tmp_arr_out[0], PROT_C_PING_RP, cmd);
				write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
			break;
			default:
			break;
		}
	}
	
	return TRUE;
}

// ------------------------------------------------------------------
BOOL parse_CNSDT(const uint8_t *arr, size_t sz) {
	
	uint8_t b;
	uint8_t value;
	uint8_t addr_rcv;
	uint8_t addr_frm;
	uint8_t size;
	uint8_t cmd;
	uint8_t resp;
	
	/* $PCNSDT,02,01,020FFF*21\r\n
	 *  |   |  |  |  | | |  | |
	 *  |   |  |  |  | | |  | +-- end sentence 
     *  |   |  |  |  | | |  +---- checksum
	 *  |   |  |  |  | | +------- value
     *  |   |  |  |  | +--------- command type
     *  |   |  |  |  +----------- size
     *  |   |  |  +-------------- address of sender
     *  |   |  +----------------- address of receiver
     *  |   +-------------------- type of sentence (header part)
     *  +------------------------ id of sender (header part)
     */
	
	if(sz < 21)
		return FALSE;
	
	addr_rcv = (ascii2hex(arr[8]) << 4) & 0xF0;
	addr_rcv |= ascii2hex(arr[9]) & 0x0F;
	
	addr_frm = (ascii2hex(arr[11]) << 4) & 0xF0;
	addr_frm |= ascii2hex(arr[12]) & 0x0F;
	
	size = (ascii2hex(arr[14]) << 4) & 0xF0;
	size |= ascii2hex(arr[15]) & 0x0F;
	
	if(sz != (21 + 2*size))
		return FALSE;
		
	cmd = (ascii2hex(arr[16]) << 4) & 0xF0;
	cmd |= ascii2hex(arr[17]) & 0x0F;
	
	value = (ascii2hex(arr[18]) << 4) & 0xF0;
	value |= ascii2hex(arr[19]) & 0x0F;
	
	if((PROT_ADDR != addr_rcv) && (PROT_BROADCAST_ADDR != addr_rcv)) {
		return FALSE;
	}
	
	switch(cmd) {
	case PROT_C_VALUE:
		set_pwm(value);
		b = prot_make_RTRDT(addr_frm, PROT_ADDR, PROT_C_ACCPT, cmd, 0, NULL, &tmp_arr_out[0]);
		write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
	break;
	case PROT_C_STOP:
		set_stop();
		b = prot_make_RTRDT(addr_frm, PROT_ADDR, PROT_C_ACCPT, cmd, 0, NULL, &tmp_arr_out[0]);
		write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
	break;
	case PROT_C_UP:
		resp = set_up() ? PROT_C_ACCPT : PROT_C_DCLN;
		b = prot_make_RTRDT(addr_frm, PROT_ADDR, resp, cmd, 0, NULL, &tmp_arr_out[0]);
		write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
	break;
	case PROT_C_DOWN:
		resp = set_down() ? PROT_C_ACCPT : PROT_C_DCLN;
		b = prot_make_RTRDT(addr_frm, PROT_ADDR, resp, cmd, 0, NULL, &tmp_arr_out[0]);
		write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
	break;
	case PROT_C_AUTO_ON:
		use_autowatering = TRUE;
		resp = PROT_C_ACCPT;
		b = prot_make_RTRMD(addr_frm, PROT_ADDR, &tmp_arr_out[0], resp, cmd);
		write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
    break;
    case PROT_C_AUTO_OFF:
    	use_autowatering = FALSE;
		resp = PROT_C_ACCPT;
		b = prot_make_RTRMD(addr_frm, PROT_ADDR, &tmp_arr_out[0], resp, cmd);
		write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
    break;
	default:
	break;
	}
	
	return TRUE;
}

// ------------------------------------------------------------------
void InitAll(void)
{
	// init buffers
	q_uart_out.arr = arr_uart_out;
	q_uart_out.offset_set = 0;
    q_uart_out.offset_get = 0;
	q_uart_in.arr = arr_uart_in;
	q_uart_in.offset_set = 0;
    q_uart_in.offset_get = 0;
	//InitUSART
	// set baudrate
	UBRRL = LO(bauddivider);
	UBRRH = HI(bauddivider);
	// 
	UCSRA = 0;
	// enable receiver and transmiter
	UCSRB = _BV(RXEN)|_BV(TXEN);
	Disable_UART_Int(TXCIE_MASK|UDRIE_MASK);
	Enable_UART_Int(RXCIE_MASK);
	UCSRC = /*1<<URSEL|*/1<<UCSZ0|1<<UCSZ1;
	// Timer 0
	TCCR0A = _BV(WGM01) | _BV(WGM00) | _BV(COM0A1);
	TCCR0B = TCCR0B_PRESC_256;	    // Prescaler
	TCNT0 = 0;						// Установить начальное значение счётчиков
	OCR0A  = 255/3; 			    // Установить значение в регистр сравнения
	//TIMSK0 |= _BV(OCIE0A);			// Разрешаем прерывание RTOS - запуск ОС
	// Timer 2
	TCCR2A = _BV(WGM21);			// CTC Mode
	TCCR2B = TCCR2B_PRESC_64/*UNET_PRESC*/;		    // Prescaler
	TCNT2  = 0;
	OCR2A  = TimerDivider/*UNET_COMP*/;  // Установить значение в регистр сравнения
	TIMSK2 |= _BV(OCIE2A);
	// led
	LED_DDR |= 1 << STATUS_LED;		// 
	// probe power pin
	PROBEPWR_PORT &= ~_BV(PROBEPWR_PIN);
	PROBEPWR_DDR |= _BV(PROBEPWR_PIN);		// charge output
	// ADC
	ADMUX = 0;
	ADMUX |= _BV(REFS0) | _BV(ADLAR) | _BV(MUX2) | _BV(MUX1) | _BV(MUX0);	// ADC7
	ADCSRA = _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);	// prescaler
	ADCSRA |= _BV(ADEN) /*| _BV(ADSC)*/;	// включаем АЦП
	// Unet task
	if(!unet_init(unet_handlr))
		SwonLED(LED_FAIL);

	SENSE_DDR |= _BV(SENSE_PIN);
	SENSE_PORT &= ~_BV(SENSE_PIN);
	
	// pwm
	EN_DDR |= _BV(EN_PIN);
	//EN_PORT |= _BV(EN_PIN);
	IN1_PORT &= ~_BV(IN1_PIN);
	IN1_DDR |= _BV(IN1_PIN);
	IN2_PORT &= ~_BV(IN2_PIN);
	IN2_DDR |= _BV(IN2_PIN);

	// prot
	sents[0].header = "PCNSMD";
	sents[0].func = parse_CNSMD;
	sents[1].header = "PCNSDT";
	sents[1].func = parse_CNSDT;
	prot_init(PROT_ADDR, &sents[0], SENT_SIZE);
}

// ------------------------------------------------------------------
int main(void)
{
	InitAll();
	InitRTOS();
	tm_reg_task(TSK_DIOD, DiodeOnTask);
	tm_reg_task(TSK_PROT, protCollectTask);
	tm_reg_task(TSK_SRV, ServiceTask);
	tm_reg_task(TSK_MOISTURE, MoistureProbePowerTask);
	tm_reg_task(TSK_UNET_DEQUEUE, unet_deque_task);
	tm_reg_task(TSK_UNET_RECEIVE, receive_func);
	tm_reg_task(TSK_UNET_HANDLE, unet_timer_handle);
	//
	// Diod task
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
	// устанавливаем отложенный вызов задачи протокола обмена
	SetTimerTask(TASK_PTR(TSK_PROT), 10);
	//
	SetTimerTask(TASK_PTR(TSK_SRV), 1);
	//
	//SetTimerTask(TASK_PTR(TSK_MOISTURE), 1000);

	Enable_Interrupt				// разрешаем прерывания
	tm_exec();
	return 0;
}