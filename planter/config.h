
#define UNET_ADDR 0x12
#define PROT_ADDR UNET_ADDR
// Port B
#define IN2_DDR     DDRB
#define IN2_PORT    PORTB
#define IN2_PIN     0
#define SENSE_DDR   DDRB
#define SENSE_PORT  PORTB
#define SENSE_PIN	1
// Port C
#define DISCHARGE_PORT PORTC
#define DISCHARGE_DDR DDRC
#define DISCHARGE_PIN 0
#define PROBEPWR_PORT PORTC
#define PROBEPWR_DDR DDRC
#define PROBEPWR_PIN 1
#define LED_DDR 	DDRC
#define LED_PORT 	PORTC
#define LED_PORT_RO PINC
#define STATUS_LED 	2
#define UNET_PORT 	PORTC
#define UNET_PORT_RO PINC
#define UNET_DDR 	DDRC
#define UNET_PIN 	3
// measure pin acquired by ADC
// Port D
#define IN1_DDR     DDRD
#define IN1_PORT    PORTD
#define IN1_PIN     7
#define EN_DDR		DDRD
#define EN_PORT		PORTD
#define EN_PIN		6


// tasks id
// 0 prohibited
#define TSK_UNET_HANDLE  1
#define TSK_UNET_DEQUEUE 2
#define TSK_UNET_RECEIVE 3
#define TSK_DIOD  		 4
#define TSK_MOISTURE	 5
#define TSK_SRV			 6
#define TSK_PROT         7	// задача обмена по протоколу

//
//#define swon_unet_led LED_PORT |= _BV(STATUS_LED)

//#define swoff_unet_led LED_PORT &= ~_BV(STATUS_LED)
