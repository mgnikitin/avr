/*
 * skeleon project
 */ 

#include "config.h"
#include "task_mngr.h"
#include "queuebuf.h"
#include "routing.h"
#include "asmroutine.h"
//

#define LED_FAIL STATUS_LED

#define UART_BUFFER_SIZE 32	// use &

// массив байт UART на отправку
static uint8_t arr_uart_out[UART_BUFFER_SIZE];
// массив байт, считанных с UART
static uint8_t arr_uart_in[UART_BUFFER_SIZE];

static volatile t_que q_uart_out;
static volatile t_que q_uart_in;

#define SwonLED(led) LED_PORT |= _BV(led)	 	/* логичеcкая единица */
#define SwoffLED(led) LED_PORT &= ~_BV(led)		/* логический ноль */
#define IsLED(led) (LED_PORT_RO & _BV(led))

void swon_unet_led(void) {
	SwonLED(STATUS_LED);
}

void swoff_unet_led(void) {
	SwoffLED(STATUS_LED);
}

extern void DiodeOffTask(void);
extern void uart_log(uint8_t);


//RTOS Interrupt
ISR(RTOS_ISR) {
	TASK_PTR(TSK_TR_SRV)->active = 1;
}

/* USART Rx Complete */
ISR(USART_RX_vect) {
	// принудительно записываем в буфер,
	// если в буфере нет места. первый элемент
	// будет удален
	q_uart_in.arr[q_uart_in.offset_set] = UDR;
	q_uart_in.offset_set = (q_uart_in.offset_set + 1) & (UART_BUFFER_SIZE - 1);
//	if(q_uart_in.offset_set == q_uart_in.offset_get)
		//q_uart_in.offset_get = (q_uart_in.offset_get + 1) & (UART_BUFFER_SIZE - 1);
}

/* USART, Data Register Empty */
ISR(USART_UDRE_vect) {
	UDR = q_uart_out.arr[q_uart_out.offset_get];
	q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);
	// очередь пуста, запрещаем прерывания по опустошению буфера передачи UDR UART
	if(q_uart_out.offset_get == q_uart_out.offset_set)
		Disable_UART_Int(UDRIE_MASK);
}

/* USART Tx Complete */
ISR(USART_TX_vect) {
	
}

void write_to_uart_buf(const uint8_t *arr, uint8_t sz) {
	uint8_t i;
	for(i = 0; i < sz; i++)
	{
		uart_log(arr[i]);
	}
}

void uart_log(uint8_t b) {
	uint8_t sreg = STATUS_REG;
	Disable_Interrupt
	
	q_uart_out.arr[q_uart_out.offset_set] = b;
	q_uart_out.offset_set = (q_uart_out.offset_set + 1) & (UART_BUFFER_SIZE - 1);
	if(q_uart_out.offset_set == q_uart_out.offset_get)
		q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);

	STATUS_REG = sreg;
	Enable_UART_Int(UDRIE_MASK);
}

/*
void DiodeOnTask(void) {
	SwonLED(STATUS_LED);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOffTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}

void DiodeOffTask(void) {
	SwoffLED(STATUS_LED);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOnTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}
*/

// ------------------------------------------------------------------
void InitAll(void)
{
	// init buffers
	q_uart_out.arr = arr_uart_out;
	q_uart_out.offset_set = 0;
    q_uart_out.offset_get = 0;
	q_uart_in.arr = arr_uart_in;
	q_uart_in.offset_set = 0;
    q_uart_in.offset_get = 0;
	//InitUSART
	// set baudrate
	UBRRL = LO(bauddivider);
	UBRRH = HI(bauddivider);
	// 
	UCSRA = 0;
	// enable receiver and transmiter
	UCSRB = _BV(RXEN)|_BV(TXEN);
	Disable_UART_Int(TXCIE_MASK|UDRIE_MASK);
	Enable_UART_Int(RXCIE_MASK);
	UCSRC = /*1<<URSEL|*/1<<UCSZ0|1<<UCSZ1;
	// Timer 0
	TCCR0A = 1<<WGM01;				// CTC Mode ( Автосброс после достижения регистра сравнения)
	TCCR0B = TCCR0B_PRESCALER;	    // Prescaler
	TCNT0 = 0;						// Установить начальное значение счётчиков
	OCR0A  = TimerDivider; 			// Установить значение в регистр сравнения
	TIMSK0 |= _BV(OCIE0A);			// Разрешаем прерывание RTOS - запуск ОС
	// led
	LED_DDR |= 1 << STATUS_LED;		// 
}

// ------------------------------------------------------------------
int main(void)
{
	InitAll();
	InitRTOS();
	//tm_reg_task(TSK_DIOD, DiodeOnTask);
	//
	// Diod task
	//SetTimerTask(TASK_PTR(TSK_DIOD), 1000);

	Enable_Interrupt				// разрещаем прерывания
	tm_exec();
	return 0;
}