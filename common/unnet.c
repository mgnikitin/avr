/**
  * version 0.6
  * 26.03.2015
  */
#include "unnet.h"
#include <string.h>

extern void uart_log(uint8_t);	// TODO

#define T_INTERVAL_MIN 1
// размер буфера на прием
#define BUFFER_SZ 20
// размер стека буфера отложенных пакетов для передачи
#define UNET_PCKT_STACK_SZ 10

// минимальный временной интервал инициализации начала передачи
#define INIT_READING_MIN (10 * T_INTERVAL_MIN)
#define INIT_READING_MAX (20 * T_INTERVAL_MIN)

// максимальный временной интервал синхронизирующего импульса
#define SYNC_DELAY_MAX (2 * T_INTERVAL_MIN)

#define LOGIC_ZERO_MIN T_INTERVAL_MIN
#define LOGIC_ONE_MIN (3 * T_INTERVAL_MIN)
#define LOGIC_OVERFLOW (6 * T_INTERVAL_MIN)

static uint8_t bit_shift = 0;				// смещение бита на чтение (запись)
static uint8_t databyte = 0;
//static RECEIVE_STATE rcv_st = RV_NONE; 		// состояние приема (чему соответствует принимаемый байт)

static uint8_t sz_data_in = 0;
//static uint8_t arr_data_in[DATA_FLD_SZ];	// массив поля Дата пакета на прием
static t_unet_pckt unet_pckt_rv;	// пакет на прием //  = { 0, 0, 0, 0, 0, (uint8_t *)&arr_data_in, RV_NONE, 0 }

typedef struct {
	uint8_t arr_bufo[BUFFER_SZ];			// массив на отправку
	uint8_t num_bufo;						// номер отправляемого байта
	uint8_t sz_bufo;						// размер буфера на отправку
} bufo_t;

bufo_t default_bufo(void) {

	bufo_t d;
	memset((void *)d.arr_bufo, 0, BUFFER_SZ);
	d.num_bufo = 0;
	d.sz_bufo = 0;
	return d;
}

static bufo_t bufo;		// буфер на отправку

// массив пакетов unet, хранящий неотправленные пакеты
static t_unet_pckt arr_unet_pckt[UNET_PCKT_STACK_SZ];
// массив полей Дата пакета unet
//static uint8_t arr_unet_pckt_data[UNET_PCKT_STACK_SZ][DATA_FLD_SZ];
// массив смещений адресов неотправленных пакетов unet
static uint8_t arr_unet_pckt_addr_sh[UNET_PCKT_STACK_SZ];
// очередь пакетов unet на передачу (содержит адреса пакетов в памяти)
static t_queue q_unet_pckt_addr;

#define UNET_SEND_LOW  	UNET_PORT &= ~_BV(UNET_PIN)  // *pport &= ~_BV(pin)
#define IS_HIGH() UNET_REG & UNET_PIN_MASK
#define IS_LOW() !(UNET_REG & UNET_PIN_MASK)

static void empty_func(const t_unet_pckt * ppckt) {
}

unet_hanldr_t recv_func = empty_func;

uint8_t calc_crc(uint8_t *arr, uint8_t sz) {

	uint8_t cs = 0;
	uint8_t i;
	for(i = 0; i < sz; i++) {
		cs ^= arr[i];
	}
	return cs;
}

uint8_t unet_crc_pckt(const t_unet_pckt *p) {

	uint8_t cs = 0;
	cs ^= calc_crc((uint8_t *)p, 3);
	if(C_DATA == p->cmd) {
		cs ^= p->sz;
		cs ^= calc_crc((uint8_t *)p->dat, p->sz);
	}
	return cs;
}

static void parse_byte(void) {
	switch(unet_pckt_rv.rcv_st) {
	case RV_ADDR_DEST:				// состояние приема адреса назначения
		unet_pckt_rv.addr_dst = databyte;
		unet_pckt_rv.rcv_st = RV_ADDR_SRC;
	break;
	case RV_ADDR_SRC:				// состояние приема адреса отправителя
		unet_pckt_rv.addr_src = databyte;
		unet_pckt_rv.rcv_st = RV_CMD;
	break;
	case RV_CMD:					// состояние приема команды
		unet_pckt_rv.cmd = databyte;
		unet_pckt_rv.rcv_st = (C_DATA == databyte) ? RV_SIZE : RV_CRC;
	break;
	case RV_SIZE:					// состояние приема размера поля данных
		unet_pckt_rv.sz = 0;
		sz_data_in = databyte;
		// ограничение по размеру поля Дата пакета
		unet_pckt_rv.rcv_st = ((sz_data_in == 0) || (sz_data_in > DATA_FLD_SZ)) ? RV_NONE : RV_DATA;
	break;
	case RV_DATA:					// состояние приема поля данных
		unet_pckt_rv.dat[unet_pckt_rv.sz] = databyte;
		unet_pckt_rv.sz++;
		unet_pckt_rv.rcv_st = (unet_pckt_rv.sz == sz_data_in) ? RV_CRC : RV_DATA;
	break;
	case RV_CRC:
		unet_pckt_rv.crc = unet_crc_pckt(&unet_pckt_rv);
		unet_pckt_rv.rcv_crc = databyte;
		// вызываем обработчик пакета, если адрес назначения совпал 
		// с собственным или пакет передан по широковещательному адресу
		if((UNET_ADDR == unet_pckt_rv.addr_dst) 
				|| (UNET_BROADCAST_ADDR == unet_pckt_rv.addr_dst))
		{
			SetTask(TASK_PTR(TSK_UNET_RECEIVE));
		}
		unet_pckt_rv.rcv_st = RV_FINISHED;
	break;
	case RV_FINISHED:
		unet_pckt_rv.rcv_st = RV_NONE;
	break;
	default:
	case RV_NONE:
	break;
	}
}

void receive_func(void) {
	// TODO существует опасность затереть пакет следующим сообщением
	recv_func((const t_unet_pckt *)&unet_pckt_rv);
}

void unet_deque_task(void) {
	uint8_t b;
	if(ST_IDLE == (UNET_REG & ST_MASK)) 
	{
		if(deque(&q_unet_pckt_addr, &b))
		{	// есть неотправленные пакеты
			bufo.sz_bufo = 4;
			//uart_log('d');
			bufo.arr_bufo[0] = arr_unet_pckt[b].addr_dst;
			bufo.arr_bufo[1] = arr_unet_pckt[b].addr_src;
			bufo.arr_bufo[2] = arr_unet_pckt[b].cmd;
			if(C_DATA == arr_unet_pckt[b].cmd) {
				bufo.sz_bufo += arr_unet_pckt[b].sz;
				bufo.arr_bufo[3] = arr_unet_pckt[b].sz;
				memcpy(&bufo.arr_bufo[4], (const void *)arr_unet_pckt[b].dat, arr_unet_pckt[b].sz);
			}
			bufo.arr_bufo[bufo.sz_bufo-1] = calc_crc((uint8_t *)bufo.arr_bufo, bufo.sz_bufo-1);
			bufo.num_bufo = 0;
			UNET_SMP_COUNT = 0;
			UNET_REG &= ~ST_WRITE_MASK;
			UNET_REG |= ST_INIT_SENDING;
			UNET_REG |= UNET_WR_PIN_MASK;
			turn_write_macro		// меняем направление порта на запись (производим инициализацию передачи)
			bit_shift = 0;			// сбрасываем бит смещения
		}
	}
}

BOOL unet_init(unet_hanldr_t recv_hanldr) {

	uint8_t i = 0;
	UNET_REG = 0;
	recv_func = recv_hanldr;
	bufo = default_bufo();
	
	init_que(&q_unet_pckt_addr, arr_unet_pckt_addr_sh, UNET_PCKT_STACK_SZ);
	for(i = 0; i < UNET_PCKT_STACK_SZ; i++) {
		arr_unet_pckt_addr_sh[i] = i;
	}

	// устанавливаем направление порта на чтение
	turn_read_macro
	UNET_SEND_LOW;
	//
	return TRUE;
}

void unet_timer_handle(void) {

	if(ST_IDLE == (UNET_REG & ST_MASK)) {
		turn_read_macro 		// меняем направление порта на чтение 
		swoff_unet_led;
		//
		if(!(UNET_PORT_RO & _BV(UNET_PIN)))	{	// обнаружен импульс сброса
			UNET_SMP_COUNT = 0;
			UNET_REG |= ST_INIT_READING;
			unet_pckt_rv.rcv_st = RV_ADDR_DEST;			// состояние приема на чтение адреса назначения
			bit_shift = 0;			// сбрасываем бит смещения			
		}
		else if(bufo.sz_bufo != 0) {	// HIGH есть неотправленные байты
			UNET_SMP_COUNT = 0;
			UNET_REG |= ST_INIT_SENDING;
			UNET_REG |= UNET_WR_PIN_MASK;
			turn_write_macro		// меняем напрвление порта на запись (производим инициализацию передачи)
		}
		else if(q_unet_pckt_addr._count != 0) {		// high, buffer empty
			// добавляем задачу копирования из очереди в буфер
			SetTask(TASK_PTR(TSK_UNET_DEQUEUE));
			bufo.sz_bufo = 0xFF;			// помечаем, что буфер занят
		}
	}
	else if(UNET_REG & ST_READ_MASK) {			// производим чтение
		uint8_t unet_reg = UNET_REG & ST_READ_MASK;
		swon_unet_led;
		if(ST_INIT_READING == unet_reg) {	    // состояние инициализации чтения
			if(IS_HIGH()) {
				UNET_REG &= ~ST_READ_MASK;
				// если временной интервал импульса сброса
				// соответствует интервалу инициализации, то
				// переходим к чтению
				if(UNET_SMP_COUNT >= INIT_READING_MIN) {
					UNET_SMP_COUNT = 0;
					UNET_REG |= ST_SYNC_READING;
					//databyte = 0;
					bit_shift = 0;			// сбрасываем бит смещения
				}
				else
				{	// слишком короткий импульс сброса для инициализации передачи
					//ST_IDLE;
					uart_log('s');
					uart_log(0x30 + UNET_SMP_COUNT);
				}
			}
			else if(UNET_SMP_COUNT > INIT_READING_MAX) {
				// слишком длинный импульс сброса для инициализации передачи
				UNET_REG &= ~ST_MASK; //ST_IDLE;
				uart_log('S');
				uart_log(0x30 + UNET_SMP_COUNT);
			}
		}
		else if(ST_SYNC_READING == unet_reg)		// синхронизация чтения бита
		{
			if(IS_HIGH())
			{  // ждем окончания синхронизирующего импульса
				if(UNET_SMP_COUNT > SYNC_DELAY_MAX) {
					// слишком длинный синхронизирующий импульс
					UNET_REG &= ~ST_READ_MASK;
					UNET_REG |= ST_IDLE;
					uart_log('Y');
				}
			}
			else {	// переходим к чтению
				UNET_SMP_COUNT = 0;
				UNET_REG &= ~ST_READ_MASK;
				UNET_REG |= ST_READING;
			}
		}
		else if(ST_READING == unet_reg)			// чтение бита
		{
			if(IS_HIGH())
			{	// обнаружен синхронизирующий импульс
				UNET_REG &= ~ST_READ_MASK;
				if(UNET_SMP_COUNT >= LOGIC_OVERFLOW) {
					// слишком длинный интервал времени для регистрации
					// логического уровня сигнала
					UNET_REG |= ST_IDLE;
					uart_log('Q');
				}
				else if(UNET_SMP_COUNT >= LOGIC_ZERO_MIN) {
					if(UNET_SMP_COUNT >= LOGIC_ONE_MIN) {  // обнаружена 1
						databyte |= 1 << (7-bit_shift);
						//uart_log('1');
					}
					else { 		//  обнаружен 0
						databyte &= ~(1 << (7-bit_shift));
						//uart_log('0');
					}
					++bit_shift;
					bit_shift &= 7;
					if(0 == bit_shift) {
						parse_byte();
					}
				}
				else {
					// слишком короткий интервал времени для регистрации
					// логического уровня сигнала TODO
					UNET_REG |= ST_IDLE;
					uart_log('s');
				}
				UNET_SMP_COUNT = 0;
				
				// если больше не ждем поступления байта, переходим к состоянию ожидания инициализации
				if(RV_FINISHED == unet_pckt_rv.rcv_st) {
					UNET_REG |= ST_IDLE;
					unet_pckt_rv.rcv_st = RV_NONE;
				}
				else {
					UNET_REG |= ST_SYNC_READING;
				}
			}
			else
			{
				if(UNET_SMP_COUNT >= INIT_READING_MIN)
				{	// обнаружен сброс (инициализация)
					UNET_SMP_COUNT = 0;
					UNET_REG &= ~ST_READ_MASK;
					UNET_REG |= ST_INIT_READING;
					uart_log('m');
				}
			}
		}
	}
	else if(UNET_REG & ST_WRITE_MASK) {
		uint8_t unet_reg = UNET_REG & ST_WRITE_MASK;
		swon_unet_led;
		if(ST_INIT_SENDING == unet_reg)		// состояние инициализации отправки
		{	// инициализация передачи
			if(UNET_SMP_COUNT > INIT_READING_MIN) {
				UNET_SMP_COUNT = 0;
				bit_shift = 0;
				turn_read_macro
				UNET_REG &= ~UNET_WR_PIN_MASK;
				UNET_REG &= ~ST_WRITE_MASK;
				UNET_REG |= ST_SEND_SYNC;
			}
		}
		else if(ST_SEND_SYNC == unet_reg)			// состояние синхронизации записи бита
		{	// синхронизирующий импульс передачи
			if(UNET_SMP_COUNT == T_INTERVAL_MIN)
			{
				UNET_SMP_COUNT = 0;
				UNET_REG |= UNET_WR_PIN_MASK;
				//turn_write_macro
				UNET_REG &= ~ST_WRITE_MASK;
				UNET_REG |= (bufo.arr_bufo[bufo.num_bufo] & _BV(7-bit_shift)) ? ST_SEND_ONE_BIT : ST_SEND_ZERO_BIT;
			}
		//	else if(IS_LOW())
		//	{// обнаружен импульс сброса, линия занята
		//		curr_state = ST_IDLE;
		//	}
		}
		else if(ST_SEND_ONE_BIT == unet_reg)		// состояние записи логической 1
		{
			if(UNET_SMP_COUNT > LOGIC_ONE_MIN)
			{
				UNET_SMP_COUNT = 0;
				UNET_REG &= ~ST_WRITE_MASK;
				UNET_REG &= ~UNET_WR_PIN_MASK;
				//turn_read_macro
				//uart_log('1');
				++bit_shift;
				if(bit_shift > 7)		// байт отправлен
				{
					++bufo.num_bufo;			// переходим к следующему байту
					if(bufo.num_bufo != bufo.sz_bufo) {	// еще не все отправили 
						bit_shift = 0;
						UNET_REG |= ST_SEND_SYNC;
					}
					else {
						bufo.sz_bufo = 0;		// сбрасываем буфер
						UNET_REG |= ST_IDLE;
					}
				}
				else {
					UNET_REG |= ST_SEND_SYNC;
				}
			}
		}
		else if(ST_SEND_ZERO_BIT == unet_reg)		// состояние записи логического 0
		{
			if(UNET_SMP_COUNT > LOGIC_ZERO_MIN)
			{
				UNET_SMP_COUNT = 0;
				UNET_REG &= ~ST_WRITE_MASK;
				UNET_REG &= ~UNET_WR_PIN_MASK;
				//turn_read_macro
				//uart_log('0');
				++bit_shift;
				if(bit_shift > 7)		// байт отправлен
				{
					++bufo.num_bufo;			// переходим к следующему байту
					if(bufo.num_bufo != bufo.sz_bufo)	{	// еще не все отправили
						bit_shift = 0;
						UNET_REG |= ST_SEND_SYNC;
					}
					else {
						bufo.sz_bufo = 0;		// сбрасываем буфер
						UNET_REG |= ST_IDLE;
					}
				}
				else {
					UNET_REG |= ST_SEND_SYNC;
				}
			}
		}
	}
}

BOOL unet_sendb(uint8_t addr, uint8_t b)
{
	return unet_send(addr, &b, 1);
}

// отправить сообщение
BOOL unet_send(uint8_t addr, uint8_t *arr, uint8_t sz)
{
	if((UNET_REG & ST_MASK) != ST_IDLE)
		return FALSE;		// линия занята
	if((sz > (BUFFER_SZ - 3)) || (bufo.sz_bufo != 0))
		return FALSE;
	else {
		bufo.sz_bufo = sz + 3;
		bufo.arr_bufo[0] = addr;
		bufo.arr_bufo[1] = UNET_ADDR;
		memcpy((void *)((uint8_t *)bufo.arr_bufo + 2), (const void *)arr, sz);
		bufo.arr_bufo[2 + sz] = calc_crc((uint8_t *)bufo.arr_bufo, 2 + sz);
		bufo.num_bufo = 0;
		return TRUE;
	}
}

// поместить сообщение в стек
BOOL unet_enque(const t_unet_pckt *ppckt) {
	uint8_t b;
	if(acque(&q_unet_pckt_addr, &b)) {
		memcpy((void *)&arr_unet_pckt[b], (const void *)ppckt, sizeof(t_unet_pckt));
		return TRUE;
	}

	return FALSE;
}