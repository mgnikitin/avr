#ifndef _ASMROUTINE_
#define _ASMROUTINE_

#include "env.h"

#ifdef __ASSEMBLER__

#endif

#ifndef __ASSEMBLER__

extern void delay_ticks(uint8_t);

#define _delay_us(us) delay_ticks(us * F_CPU_MHz)

extern uint16_t micros(void);

extern uint16_t micros_isr(void);

#endif

#endif  /* _ASMROUTINE_ */