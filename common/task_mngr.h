#ifndef _TASK_MNGR_
#define _TASK_MNGR_

#include "types.h"
#include "env.h"
#include "queuebuf.h"

#define TASK_COUNT 8	// используется &
#define TIMER_COUNT TASK_COUNT

#define TSK_TR_SRV 	0

#define STACK_SIZE 100

typedef void (*task_func)(void);

typedef struct struct_task {
	uint8_t active;
	task_func hdlr;		// обработчик
	uint16_t time;						
	uint8_t id;
} t_task;

//extern volatile uint8_t curr_num;

extern t_task arr_sheduler[];

#define TASK_PTR(id) (arr_sheduler + id)

void SetTimerTask(t_task *pt, uint16_t new_time);

/// \brief Вызывать в основном цикле программы
extern void SetTask(t_task *pt);

/// \brief Вызывать в обработчике прерывания
extern void SetTask_isr(t_task *pt);

extern void InitRTOS(void);

extern void tm_reg_task(uint8_t id, task_func func);

extern void tm_exec(void);

#define save_context_macro		\
asm volatile (					\
	"push r0			\n\t"	\
	"in   r0, __SREG__	\n\t"	\
	"cli				\n\t"	\
	"push r0			\n\t"	\
	"push r1			\n\t"	\
	"push r2			\n\t"	\
	"push r3			\n\t"	\
	"push r4			\n\t"	\
	"push r5			\n\t"	\
	"push r6			\n\t"	\
	"push r7			\n\t"	\
	"push r8			\n\t"	\
	"push r9			\n\t"	\
	"push r10			\n\t"	\
	"push r11			\n\t"	\
	"push r12			\n\t"	\
	"push r13			\n\t"	\
	"push r14			\n\t"	\
	"push r15			\n\t"	\
	"push r16			\n\t"	\
	"push r17			\n\t"	\
	"push r18			\n\t"	\
	"push r19			\n\t"	\
	"push r20			\n\t"	\
	"push r21			\n\t"	\
	"push r22			\n\t"	\
	"push r23			\n\t"	\
	"push r24			\n\t"	\
	"push r25			\n\t"	\
	"push r26			\n\t"	\
	"push r27			\n\t"	\
	"push r28			\n\t"	\
	"push r29			\n\t"	\
	"push r30			\n\t"	\
	"push r31			\n\t"	\
	"lds  r26, curr_task->hdlr	\n\t"	\
	"lds  r27, curr_task->hdlr+1	\n\t"	\
	"in r0, __SP_L__	\n\t"	\
	"st X+, r0			\n\t"	\
	"in r0, __SP_H__	\n\t"	\
	"st X+, r0			\n\t"	\
);

#define restore_context_macro	\
asm volatile (					\
	"lds  r26, curr_task->hdlr	\n\t"	\
	"lds  r27, curr_task->hdlr+1	\n\t"	\
	"ld   r28, X+		\n\t"	\
	"out  __SP_L__, r28	\n\t"	\
	"ld   r29, X+		\n\t"	\
	"out  __SP_H__, r29	\n\t"	\
	"pop  r31			\n\t"	\
	"pop  r30			\n\t"	\
	"pop  r29			\n\t"	\
	"pop  r28			\n\t"	\
	"pop  r27			\n\t"	\
	"pop  r26			\n\t"	\
	"pop  r25			\n\t"	\
	"pop  r24			\n\t"	\
	"pop  r23			\n\t"	\
	"pop  r22			\n\t"	\
	"pop  r21			\n\t"	\
	"pop  r20			\n\t"	\
	"pop  r19			\n\t"	\
	"pop  r18			\n\t"	\
	"pop  r17			\n\t"	\
	"pop  r16			\n\t"	\
	"pop  r15			\n\t"	\
	"pop  r14			\n\t"	\
	"pop  r13			\n\t"	\
	"pop  r12			\n\t"	\
	"pop  r11			\n\t"	\
	"pop  r10			\n\t"	\
	"pop  r9			\n\t"	\
	"pop  r8			\n\t"	\
	"pop  r7			\n\t"	\
	"pop  r6			\n\t"	\
	"pop  r5			\n\t"	\
	"pop  r4			\n\t"	\
	"pop  r3			\n\t"	\
	"pop  r2			\n\t"	\
	"pop  r1			\n\t"	\
	"pop  r0			\n\t"	\
	"out __SREG__, r0	\n\t"	\
	"pop  r0			\n\t"	\
);

#endif /* _TASK_MNGR_ */