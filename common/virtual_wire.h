#ifndef _VIRTUAL_WIRE_
#define _VIRTUAL_WIRE_

#include "config.h"
#include "types.h"
#include "task_mngr.h"

/// Maximum number of bytes in a message, counting the byte count and FCS
#define VW_MAX_MESSAGE_LEN 30

#ifndef TSK_VW_PLL
#error "TSK_VW_PLL mush be defined"
#endif

typedef void (*vw_callback)(const uint8_t* buf, uint8_t len);

typedef struct {
	volatile uint8_t *pport_ro_rx;
	volatile uint8_t *pddr_rx;
	//uint8_t rx_pin;
	volatile uint8_t *pport_tx;
	volatile uint8_t *pddr_tx;
	//uint8_t tx_pin;
	vw_callback ready_rx_handle;
} t_vw_setup;

extern void vw_pll(void);

extern uint8_t vw_get_message(uint8_t* buf, uint8_t* len);

extern uint8_t vw_send(uint8_t* buf, uint8_t len);

extern void vw_set_tx_pin(uint8_t pin);
/*
extern void vw_set_rx_pin(uint8_t pin);
*/
extern void vw_set_ptt_inverted(uint8_t inverted);

extern void vw_setup(uint16_t speed, const t_vw_setup *vw_param);

extern void vw_tx_start(void);

//extern void vw_tx_stop(void);

extern void vw_rx_start(void);

extern void vw_rx_stop(void);

extern void vw_wait_tx(void);

#endif /* _VIRTUAL_WIRE_ */