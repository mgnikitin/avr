#include "prot.h"

static uint8_t arr_prot_in[PROT_BUFFER_SIZE];
volatile t_que q_prot_in;

static uint8_t tmp_arr[PROT_BUFFER_SIZE];
static size_t tmp_sz = 0;
static uint8_t sent_num = 0;

extern void uart_log(uint8_t);	// TODO
extern void write_to_uart_buf(const uint8_t *arr, uint8_t sz);

uint8_t sents_sz = 0;
t_sent *sents;
static uint8_t own_addr = 0;
const char *header = "PRTR";

uint8_t calc_cs(const uint8_t *arr, size_t sz)
{
	uint8_t cs = 0;
	uint8_t symb;
	size_t i;
    for(i = 0; i < sz; i++)
    {
		symb = arr[i];
		if((symb == '*') || (symb == '\r'))
			break;
        cs ^= symb;
    }
    return cs;
}

BOOL prot_item(void) {
	return sents[sent_num].func((const uint8_t *)&tmp_arr[0], tmp_sz);
}

BOOL parse_sentence(void)
{
	uint8_t cs;
	uint8_t csr;
	uint8_t i;
	tmp_sz = 0;
	// копируем содержимое буфера в массив
	while(TRUE)
	{
		tmp_arr[tmp_sz] = q_prot_in.arr[q_prot_in.offset_get];
		q_prot_in.offset_get = (q_prot_in.offset_get + 1) % PROT_BUFFER_SIZE;
		// достигли последнего символа предложения
		if('\n' == tmp_arr[tmp_sz]) {
			tmp_sz++;
            break;
		}
		if(q_prot_in.offset_get == q_prot_in.offset_set)
			return FALSE;
		tmp_sz++;
		if(tmp_sz >= PROT_BUFFER_SIZE)
            return FALSE;
	}

	//uart_log('x');
	
	if(tmp_sz < 10)
	{
        return FALSE;     // слишком малый размер сообщения
	}
    // расчет контрольной суммы всех символов, кроме первого и двух последних
    cs = calc_cs(&tmp_arr[1], (size_t)(tmp_sz - 1));
	csr = (ascii2hex(tmp_arr[tmp_sz - 4]) << 4) & 0xF0;
	csr |= ascii2hex(tmp_arr[tmp_sz - 3]) & 0x0F;

	// проверка контрольной суммы
    if(csr != cs)
	{
        return FALSE;
	}

	for(i = 0; i < sents_sz; i++)
    {
   //     if(strlen((const char *)sents[i].header) != 5)
   //     {
   //         continue;
   //     }
        if(memcmp(sents[i].header, &tmp_arr[1], 6) == 0)
        {
			sent_num = i;
			return TRUE; 
        }
    }
    return FALSE;
}

uint8_t prot_make_RTRMD(uint8_t addr_trg, uint8_t addr_src, uint8_t *arr, uint8_t t, uint8_t cmd)
{
	uint8_t cs;
	size_t len = 0;
	arr[len++] = '$';
	memcpy(&arr[len], header, strlen(header));
    len += strlen(header);
    memcpy(&arr[len], "MD,", 2);
    len += 2;
	arr[len++] = ',';
	hex2ascii(addr_trg, (char *)&arr[len]);
	len += 2;
	arr[len++] = ',';
	hex2ascii(addr_src, (char *)&arr[len]);
	len += 2;
	arr[len++] = ',';
	hex2ascii(t, (char *)&arr[len]);
	len += 2;
	arr[len++] = ',';
	hex2ascii(cmd, (char *)&arr[len]);
	len += 2;
    arr[len++] = '*';
    cs = calc_cs(&arr[1], (size_t)len-1);
    hex2ascii(cs, (char *)&arr[len]);
	len += 2;
	arr[len++] = '\r';
    arr[len++] = '\n';
    return len;
}

uint8_t prot_make_RTRDT(uint8_t addr_trg, uint8_t addr_src, uint8_t t,
						uint8_t cmd, uint8_t arr_sz, uint8_t *arr_data, uint8_t *arr)
{
	uint8_t cs;
	size_t len = 0;
	arr[len++] = '$';
	memcpy(&arr[len], header, strlen(header));
    len += strlen(header);
    memcpy(&arr[len], "DT", 2);
    len += 2;
	arr[len++] = ',';
	// addr_trg
	hex2ascii(addr_trg, (char *)&arr[len]);
	len += 2;
	arr[len++] = ',';
	// addr_src
	hex2ascii(addr_src, (char *)&arr[len]);
	len += 2;
	arr[len++] = ',';
	// t
	hex2ascii(t, (char *)&arr[len]);
	len += 2;
	arr[len++] = ',';
	//  cmd
	hex2ascii(cmd, (char *)&arr[len]);
	len += 2;
	arr[len++] = ',';
	// arr data
	if(arr_sz != 0) {
		memcpy((void *)&arr[len], (const void *)arr_data, arr_sz);
		len += arr_sz;
	}
    arr[len++] = '*';
    cs = calc_cs(&arr[1], (size_t)len-1);
    hex2ascii(cs, (char *)&arr[len]);
	len += 2;
	arr[len++] = '\r';
    arr[len++] = '\n';
    return len;
}

BOOL prot_parse()
{
	uint8_t i = 0;

	while(TRUE)
	{
		if(q_prot_in.offset_get == q_prot_in.offset_set)
			return FALSE;
		if(q_prot_in.arr[q_prot_in.offset_get] != '$') {
			q_prot_in.offset_get = (q_prot_in.offset_get + 1) % PROT_BUFFER_SIZE;
			continue;
		}
		else
			break;
	}

	if(q_prot_in.arr[q_prot_in.offset_get] == '$') {
		i = q_prot_in.offset_get;
		while(TRUE) {
			i = (i + 1) % PROT_BUFFER_SIZE;
			if(i == q_prot_in.offset_set)
				return FALSE;
			
			if(q_prot_in.arr[i] == '\n') {
				return TRUE;
			}
		}
	}

	return FALSE;
}

void prot_init(uint8_t addr, t_sent *snt, uint8_t snt_sz)
{
	q_prot_in.arr = arr_prot_in;
	q_prot_in.offset_set = 0;
	q_prot_in.offset_get = 0;
	
	own_addr = addr;
	sents = snt;
	sents_sz = snt_sz;
}