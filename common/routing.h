#include "types.h"

extern uint8_t saved_STATUS_REG;

#define safe_cli_macro() \
	saved_STATUS_REG = STATUS_REG;	\
	Disable_Interrupt

#define sei_restore_macro()	\
	STATUS_REG = saved_STATUS_REG;

	
#define safe_Disable_Interrupt safe_cli_macro()
#define safe_Enable_Interrupt sei_restore_macro()

	
uint8_t ascii2hex(uint8_t);

void hex2ascii(uint8_t hx, char *str);