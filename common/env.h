#ifndef _ENV_
#define _ENV_

#include <avr/io.h>
#include <avr/interrupt.h>
//#include <avrlibtypes.h>
//#include <avrlibdefs.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#include "types.h"

#define STATUS_REG 			SREG
#define Interrupt_Flag		SREG_I
#define Disable_Interrupt	cli();
#define Enable_Interrupt	sei();
#define Is_Interrupt 		(STATUS_REG & _BV(Interrupt_Flag))

#define INLINE __attribute__((always_inline))

// ----- UART -----
#define UDR UDR0
#define UBRR UBRR0
#define UBRRL UBRR0L
#define UBRRH UBRR0H
#define UCSRA UCSR0A
#define UCSRB UCSR0B
#define UCSRC UCSR0C
#define TXEN TXEN0
#define RXEN RXEN0
#define UDRE UDRE0
#define UDRIE UDRIE0
#define TXCIE TXCIE0
#define RXCIE RXCIE0
#define UCSZ0 UCSZ00
#define UCSZ1 UCSZ01

#define UDRIE_MASK (1<<UDRIE)
#define RXCIE_MASK (1<<RXCIE)
#define TXCIE_MASK (1<<TXCIE)

#define TCCR0B_PRESC_8 _BV(CS01)					// Freq = CK/8
#define TCCR0B_PRESC_64 (_BV(CS01) | _BV(CS00))		// Freq = CK/64
#define TCCR0B_PRESC_256 _BV(CS02)					// Freq = CK/256
#define TCCR1B_PRESC_64 (_BV(CS11) | _BV(CS10))		// Freq = CK/64 
#define TCCR2B_PRESC_8 _BV(CS21)					// Freq = CK/8
#define TCCR2B_PRESC_64 _BV(CS22)					// Freq = CK/64
#define TCCR2B_PRESC_128 _BV(CS22) | _BV(CS20)		// Freq = CK/128
#define TCCR2B_PRESC_256 _BV(CS22) | _BV(CS21)		// Freq = CK/256
#define TCCR2B_PRESC_1024 _BV(CS22) | _BV(CS21) | _BV(CS20)		// Freq = CK/1024

#define Enable_UART_Int(flags) UCSRB |= flags
#define Disable_UART_Int(flags) UCSRB &= ~(flags)
// ----------------

#define RTOS_ISR  			TIMER0_COMPA_vect 

//Clock Config
#ifndef F_CPU
#define F_CPU 16000000L
#endif
#define F_CPU_MHz 16


//System Timer Config
#define Prescaler	  	64L
#define TCCR0B_PRESCALER TCCR0B_PRESC_64
#define	TimerDivider  		(F_CPU/Prescaler/1000L)		//

#define baudrate 9600L
#define bauddivider ((F_CPU+baudrate*8)/(baudrate*16)-1)

#define HI(x) ((x)>>8)
#define LO(x) ((x)& 0xFF)

#define UNET_PRESC TCCR2B_PRESC_8
#define UNET_COMP  (F_CPU/8/10000L)

#define CURR_TASK_NUM GPIOR2

enum STATUS_RTOS
{
	TASK_QUEUE_OK,
	TASK_QUEUE_OVERFLOW
};



#endif  // _ENV_