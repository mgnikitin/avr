#ifndef _LIST_H_
#define _LIST_H_

#include "types.h"

#define EMPTY_NUM 0xFF

typedef struct {
	uint8_t next;
	uint8_t item;
	uint8_t prev;
} t_list_item;

typedef struct {
	volatile t_list_item *arr;
	uint8_t sz;
	uint8_t idx_last;
	uint8_t idx_first;
	uint8_t heap;
} t_list;

extern void list_init(volatile t_list *lst, volatile t_list_item *arr, uint8_t sz);

extern BOOL list_append(volatile t_list *lst, uint8_t b);

extern BOOL list_acquire(volatile t_list *lst, volatile uint8_t *b);

extern uint8_t list_insert(volatile t_list *lst, uint8_t b, uint8_t before);

extern uint8_t list_insert_acq(volatile t_list *lst, volatile uint8_t *b, uint8_t before);

extern void list_take_at(volatile t_list *lst, uint8_t num);

#endif /* _LIST_H_ */