#ifndef __DEF_PROT__
#define __DEF_PROT__

#include "env.h"
#include "queuebuf.h"
#include "routing.h"
#include <string.h>

#define PROT_BUFFER_SIZE 100
#define PROT_BROADCAST_ADDR 0xFF

// идентификаторы команд (соответствуют unnet.h)
#define PROT_C_NONE		(uint8_t)0x00     //
#define PROT_C_PING_RQ	(uint8_t)0x01   // пинг устройства
#define PROT_C_PING_RP 	(uint8_t)0x02   // ответ на команду пинг
#define PROT_C_ACCPT	(uint8_t)0x03     // команда принята 
#define PROT_C_SWON_L 	(uint8_t)0x04   // включить свет
#define PROT_C_SWOFF_L 	(uint8_t)0x05   // выключить свет
#define PROT_C_ALM_W 	(uint8_t)0x10     //
#define PROT_C_HMDTY	(uint8_t)0x11     // показание влажности 
#define PROT_C_DCLN   (uint8_t)0x12     // команда отклонена
#define PROT_C_REQ_ST	(uint8_t)0x20
#define PROT_C_TEMPER	(uint8_t)0x21
#define PROT_C_STATUS	(uint8_t)0x22
#define PROT_C_UP		(uint8_t)0x23
#define PROT_C_DOWN		(uint8_t)0x24
#define PROT_C_VALUE	(uint8_t)0x25
#define PROT_C_START	(uint8_t)0x26     // старт
#define PROT_C_STOP 	(uint8_t)0x27     // стоп
#define PROT_C_CNT    (uint8_t)0x28     //
#define PROT_C_AUTO_ON  (uint8_t)0x29   // перейти в автоматический режим
#define PROT_C_AUTO_OFF (uint8_t)0x30   // выйти из автоматического режима
#define PROT_C_DATA		(uint8_t)0xF0

typedef struct
{
	const char *header;
	BOOL (*func)(const uint8_t *arr, size_t sz);
} t_sent;

extern volatile t_que q_prot_in;

extern BOOL prot_item(void);

extern BOOL parse_sentence(void);

extern BOOL prot_parse(void);

/** Передать команду (ответ на команду)
  * addr_trg - адрес назначения
  * addr_src - адрес источника
  * arr - сформированный массив данных (выходной параметр)
  * t - тип сообщения (ответ)
  * cmd - запрашиваемая команда (входная команда)
  */
extern uint8_t prot_make_RTRMD(uint8_t addr_trg, uint8_t addr_src, uint8_t *arr, 
  uint8_t t, uint8_t cmd);

/** Передать пакет с данными
  * addr_trg - адрес назначения
  * addr_src - адрес источника
  * t - тип сообщения (ответ)
  * cmd - запрашиваемая команда (входная команда)
  * arr_sz - размер массива данных
  * arr_data - массив данных (входной параметр)
  * arr - сформированный массив (выходной параметр)
  */
extern uint8_t prot_make_RTRDT(uint8_t addr_trg, uint8_t addr_src, uint8_t t,  
								uint8_t cmd, uint8_t arr_sz, uint8_t *arr_data, uint8_t *arr);

extern void prot_init(uint8_t addr, t_sent *snt, uint8_t snt_sz);

#endif // __DEF_PROT__
