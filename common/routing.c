#include "routing.h"

uint8_t saved_STATUS_REG;


uint8_t ascii2hex(uint8_t a) {
	if(a >= 'a')
		return a - 0x57;
	else if(a >= 'A')
		return a - 0x37;
	else
		return a - 0x30;
}

void hex2ascii(uint8_t hx, char *str) {
	
	uint8_t h = (hx >> 4) & 0x0F;
	uint8_t l = hx & 0x0F;
	
	str[0] = (h > 9) ? (h + 0x37) : (h + 0x30);
	str[1] = (l > 9) ? (l + 0x37) : (l + 0x30);
}
