/**
  * version 0.3
  * 15.03.2015
  */
#ifndef __QUEUEBUF__
#define __QUEUEBUF__

#include "types.h"

typedef struct {
	uint8_t offset_set; 	// смещение позиции записи
    uint8_t offset_get; 	// смещение позиции чтения
    uint8_t _count;    		// размер занятой части буфера
    uint8_t sz;   			// размер буфера
    uint8_t *arr;
} t_queue;

typedef struct {
	uint8_t offset_set; 	// смещение позиции записи
    uint8_t offset_get; 	// смещение позиции чтения
	uint8_t *arr;
} t_que;

extern void init_que(volatile t_queue *pq,  uint8_t *arr, uint8_t sz);

extern BOOL enque(volatile t_queue *pq, uint8_t b);

// принудительная запись в очередь
extern void enque_f(volatile t_queue *pq, uint8_t b);

// ------------------------------------------------------------------
extern BOOL acque(volatile t_queue *pq, volatile uint8_t *b);

// ------------------------------------------------------------------
#define acque_macro(pq, b, rez) 	\
	if(pq._count < pq.sz)			\
    {								\
		b = pq.arr[pq.offset_set];	\
		pq.offset_set = ++pq.offset_set % pq.sz;	\
        ++pq._count;				\
		rez = TRUE;					\
	}								\
	else							\
		rez = FALSE;

extern BOOL deque(volatile t_queue *pq, volatile uint8_t *b);

extern BOOL deque_n(volatile t_queue *pq);

// поиск массива в буфере, буфер очищается пока не совпадут массивы
extern BOOL qfind(volatile t_queue *pq, const uint8_t *arr, uint8_t size);

// поиск массива в буфере, буфер остается нетронутым
extern BOOL qfind_silent(volatile t_queue *pq, const uint8_t *arr, uint8_t size);

#endif // __QUEUEBUF__