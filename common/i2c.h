#ifndef __DEF_I2C__
#define __DEF_I2C__

#include "env.h"
#include "queuebuf.h"

#define I2C_ST_SUCCESS   0
#define I2C_ST_ERR_CL    8
#define I2C_ST_ERR_NA	 9
#define I2C_ST_ERR_BUS  10 

extern volatile t_queue q_i2c_log;

extern void i2c_handler(void);

extern BOOL i2c_write_master_buf(const uint8_t *arr, uint8_t sz, void (*handlr)(uint8_t));

extern BOOL i2c_read_slave_buf(uint8_t *b);

extern void i2c_init(uint8_t i2c_PORT, uint8_t i2c_DDR, uint8_t i2c_SCL, uint8_t i2c_SDA);

/// инициализация устройства i2c в режиме slave
extern void i2c_init_slave(uint8_t addr, BOOL use_brodcast, void (*handlr)(void));

#endif //  __DEF_I2C__