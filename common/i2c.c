#include "i2c.h"

#define I2C_BUFFER_SIZE 255

#define I2C_BUSY_MSK      0b01000000
#define I2C_FREE_MSK      0b10111111
#define I2C_WRITE_MSK     0b00100000  
#define I2C_INTR_MSK      0b10000000

uint8_t slave_addr = 0x10;	// TODO
static uint8_t i2c_st = 0;
static const uint8_t i2c_i_am_slave = 1;
static uint8_t i2c_slave_bufin[I2C_BUFFER_SIZE];	
static uint8_t i2c_slave_bufout[I2C_BUFFER_SIZE];	
static uint8_t i2c_b;
#define LOG_BUFFER_SIZE 100
volatile t_queue q_i2c_log;
static uint8_t arr_i2c_log[LOG_BUFFER_SIZE];
static volatile t_queue q_sl_bufin;			// буфер slave на чтение
static volatile t_queue q_sl_bufout;		// буфер slave на запись
static volatile t_queue q_ms_bufin;			// буфер master на чтение
static volatile t_queue q_ms_bufout;		// буфер master на запись

extern void uart_log_handlr(void);

static void empty_func(void)
{
}

static void empty_func_st(uint8_t st)
{
}


void (*slave_func)(void) = empty_func;
void (*master_func)(uint8_t st) = empty_func_st;

BOOL i2c_write_master_buf(const uint8_t *arr, uint8_t sz, void (*handlr)(uint8_t))
{
	if(!(i2c_st | I2C_BUSY_MSK))
	{
		master_func = handlr;
		i2c_st |= I2C_BUSY_MSK | I2C_WRITE_MSK;
		return TRUE;
	}
	return FALSE;
}

BOOL i2c_read_slave_buf(uint8_t *b)
{
	BOOL rez;
	TWCR &= ~(1<<TWIE);
	rez = deque(&q_sl_bufin, b);
	TWCR |= (1<<TWIE);
	return rez;
}

inline INLINE void i2c_handler(void)
{
//	enque(&q_i2c_log, TWSR & 0xF8);
	
	
	switch(TWSR & 0xF8)						// Отсекаем биты прескалера
	{
	case 0x00:	// Bus Fail
		TWCR = 0<<TWSTA|1<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;
		master_func(I2C_ST_ERR_BUS);
		i2c_st &= I2C_FREE_MSK;
	break;
	case 0x08:	// передан Start
		i2c_st &= ~I2C_INTR_MSK;
		if(i2c_st & I2C_WRITE_MSK)
			TWDR = 0xFE & slave_addr;		// Addr+W
		else
			TWDR = 0x01 | slave_addr;		// Addr+R
		TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;
	break;
	case 0x10:	// передан Restart
		i2c_st &= ~I2C_INTR_MSK;
		if(i2c_st & I2C_WRITE_MSK)
			TWDR = 0xFE & slave_addr;		// Addr+W
		else
			TWDR = 0x01 | slave_addr;		// Addr+R
		TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;
	break;
	case 0x18:	// передан SLA+W, получили ACK
		if(deque(&q_ms_bufout, &i2c_b))
		{
			TWDR = i2c_b;
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;
		}
		else
		{
			TWCR = 0<<TWSTA|1<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;	// передача Stop
			master_func(I2C_ST_SUCCESS);
			i2c_st &= I2C_FREE_MSK;
		}
	break;
	case 0x20:	// передан SLA+W, получили NACK
		TWCR = 0<<TWSTA|1<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;	// передача Stop
		master_func(I2C_ST_ERR_NA);
		i2c_st &= I2C_FREE_MSK;
	break;
	case 0x28:	// Byte+ACK
		if(deque(&q_ms_bufout, &i2c_b))
		{
			TWDR = i2c_b;
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;
		}
		else
		{
			TWCR = 0<<TWSTA|1<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;	// передача Stop
			master_func(I2C_ST_SUCCESS);
			i2c_st &= I2C_FREE_MSK;
		}
	break;
	case 0x30:	// Byte+NACK 
		TWCR = 0<<TWSTA|1<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;		// передача Stop
		master_func(I2C_ST_ERR_NA);
		i2c_st &= I2C_FREE_MSK;
	break;
	case 0x38:	// Collision
		TWCR = 0<<TWSTA|1<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;		// передача Stop
		master_func(I2C_ST_ERR_CL);
		i2c_st &= I2C_FREE_MSK;
	break;
	case 0x40:	// передан SLA+R, получен ACK
		TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;	// передать ACK
	break;
	case 0x48:	// переданSLA+R, получен NACK
		TWCR = 0<<TWSTA|1<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;			// передача Stop
		master_func(I2C_ST_ERR_NA);
		i2c_st &= I2C_FREE_MSK;
	break;
	case 0x50:	// Receive Byte
		enque(&q_ms_bufin, TWDR);
		if((I2C_BUFFER_SIZE - q_ms_bufin._count) == 1) // свободен 1 байт в буфере
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|0<<TWEA|1<<TWEN|1<<TWIE;	// передать NACK
		else
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;	// передать ACK
	break;
	case 0x58:	// Receive Byte+NACK (приняли последний байт)
		enque(&q_ms_bufin, TWDR);
		TWCR = 0<<TWSTA|1<<TWSTO|1<<TWINT|i2c_i_am_slave<<TWEA|1<<TWEN|1<<TWIE;			// Передали Stop
		master_func(I2C_ST_SUCCESS);
		i2c_st &= I2C_FREE_MSK;
	break;
	// Slave Receive
	case 0x60:	// получен SLA+W
	case 0x70:	// получен SLA+W по широковещательному адресу
		i2c_st |= I2C_BUSY_MSK;
		if((I2C_BUFFER_SIZE - q_sl_bufin._count) == 1)
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|0<<TWEA|1<<TWEN|1<<TWIE;	// передать NACK
		else
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;	// передать ACK
	break;
	case 0x68:	// арбитраж проигран другим мастером во время передачи SLA+W, получен SLA+W
	case 0x78:	// арбитраж проигран другим мастером во время передачи SLA+W, получен широковещательный SLA+W
		i2c_st |= I2C_INTR_MSK;
		TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;	// передать ACK
	break;
	case 0x80:	// принят байт
	case 0x90:	// принят байт по широковещательному адресу
		enque(&q_sl_bufin, TWDR);
		if((I2C_BUFFER_SIZE - q_sl_bufin._count) == 1) // свободен 1 байт в буфере
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|0<<TWEA|1<<TWEN|1<<TWIE;		// не хватает места в буфере, передать NACK
		else
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;		// передать ACK
	break;
	case 0x88:	// принят послений байт
	case 0x98:	// принят послений байт по широковещательному адресу
		enque(&q_sl_bufin, TWDR);
		if(i2c_st & I2C_INTR_MSK)
			TWCR = 1<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;
		else
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;		// передать ACK
		slave_func();
	break;
	case 0xA0:	// получен Stop или Restart
		TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;
		slave_func();
	break;
	// Slave Transmit
	case 0xA8:	// получен SLA+R
		if(deque(&q_sl_bufout, &i2c_b))
		{
			TWDR = i2c_b;
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;
		}
		else
		{
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|0<<TWEA|1<<TWEN|1<<TWIE;
		}
	break;
	case 0xB0:	// 
		i2c_st |= I2C_INTR_MSK;
		if(deque(&q_sl_bufout, &i2c_b))
		{
			TWDR = i2c_b;
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;
		}
		else
		{
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|0<<TWEA|1<<TWEN|1<<TWIE;
		}	
	break;
	case 0xB8:	// байт передан, получен ACK
		deque(&q_sl_bufout, &i2c_b);
		TWDR = i2c_b;
		if(q_sl_bufout._count == 0)
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|0<<TWEA|1<<TWEN|1<<TWIE;
		else
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;
	break;
	case 0xC0:	// 
	case 0xC8:	// 
		if(i2c_st & I2C_INTR_MSK)
			TWCR = 1<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;
		else
			TWCR = 0<<TWSTA|0<<TWSTO|1<<TWINT|1<<TWEA|1<<TWEN|1<<TWIE;
	break;
	default:
		
	break;
	}
}

void i2c_init(uint8_t i2c_PORT, uint8_t i2c_DDR, 
			  uint8_t i2c_SCL, uint8_t i2c_SDA)
{
	i2c_PORT |= 1<<i2c_SCL|1<<i2c_SDA;			// Включим подтяжку на ноги
	i2c_DDR &=~(1<<i2c_SCL|1<<i2c_SDA);
	
	init_que(&q_i2c_log, arr_i2c_log, LOG_BUFFER_SIZE);
	init_que(&q_sl_bufin, i2c_slave_bufin, I2C_BUFFER_SIZE);
	init_que(&q_sl_bufout, i2c_slave_bufout, I2C_BUFFER_SIZE);
	
	// TODO ??
	TWBR = 0xFF;         						// Настроим битрейт
	TWSR = 0x03;
}

void i2c_init_slave(uint8_t addr, BOOL use_brodcast, void (*handlr)(void))
{
	TWAR = 0xFF & (addr << 1); //addr & 0xFE;		// адрес устройства
	slave_func = handlr;
	if(use_brodcast)
		TWAR |= 0x01;		// отвечать на general call addraess (0x00)
	else
		TWAR &= 0xFE;
	// разрешаем прерывания TWI, разрешение ответа ACK
	TWCR = 0<<TWSTA | 0<<TWSTO | 1 << TWEN | 1 << TWEA | 1<<TWIE;
	TWCR &= ~(0 << TWINT);
}