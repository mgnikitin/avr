#ifndef _TYPES_H_
#define _TYPES_H_

#ifndef __ASSEMBLER__
typedef unsigned char uint8_t;
typedef uint8_t BOOL;
#endif

#define TRUE (uint8_t)1
#define FALSE (uint8_t)0

#endif /* _TYPES_H_ */