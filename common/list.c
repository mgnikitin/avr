#include "list.h"

void list_init(volatile t_list *lst, volatile t_list_item *arr, uint8_t sz) {
	uint8_t i;
	lst->arr = arr;
	lst->sz = sz;
	lst->idx_last = EMPTY_NUM;
	lst->idx_first = EMPTY_NUM;
	lst->heap = EMPTY_NUM;

	for(i = 0; i < sz; i++)
	{
		arr[i].next = lst->heap;
		lst->heap = i;
	}
}

BOOL list_append(volatile t_list *lst, uint8_t b) {
	if(lst->heap != EMPTY_NUM)
	{
		uint8_t acrd = lst->heap;
		lst->heap = lst->arr[lst->heap].next;
		//
		lst->arr[acrd].item = b;
		lst->arr[acrd].next = EMPTY_NUM;
		if(lst->idx_last != EMPTY_NUM)
		{
			lst->arr[acrd].prev = lst->idx_last;
			lst->arr[lst->idx_last].next = acrd;
		}
		else
			lst->arr[acrd].prev = EMPTY_NUM;
		lst->idx_last = acrd;
		if(lst->idx_first == EMPTY_NUM)
			lst->idx_first = lst->idx_last;
		return TRUE;
	}
	return FALSE;
}

BOOL list_acquire(volatile t_list *lst, volatile uint8_t *b) {
	if(lst->heap != EMPTY_NUM)
	{
		uint8_t acrd = lst->heap;
		lst->heap = lst->arr[lst->heap].next;
		//
		*b = lst->arr[acrd].item;
		lst->arr[acrd].next = EMPTY_NUM;
		if(lst->idx_last != EMPTY_NUM)
		{
			lst->arr[acrd].prev = lst->idx_last;
			lst->arr[lst->idx_last].next = acrd;
		}
		else
			lst->arr[acrd].prev = EMPTY_NUM;
		lst->idx_last = acrd;
		if(lst->idx_first == EMPTY_NUM)
			lst->idx_first = lst->idx_last;
		return TRUE;
	}
	return FALSE;
}

uint8_t list_insert(volatile t_list *lst, uint8_t b, uint8_t before) {
	if(lst->heap != EMPTY_NUM)
	{
		uint8_t acrd = lst->heap;
		lst->heap = lst->arr[lst->heap].next;
		//
		lst->arr[acrd].item = b;
		lst->arr[acrd].next = before;
		lst->arr[acrd].prev = lst->arr[before].prev;
		if(EMPTY_NUM != lst->arr[before].prev)
			lst->arr[lst->arr[before].prev].next = acrd;
		else
			lst->idx_first = acrd;
		lst->arr[before].prev = acrd;		
		return acrd;
	}
	return EMPTY_NUM;
}

uint8_t list_insert_acq(volatile t_list *lst, volatile uint8_t *b, uint8_t before) {
	if(lst->heap != EMPTY_NUM)
	{
		uint8_t acrd = lst->heap;
		lst->heap = lst->arr[lst->heap].next;
		//
		*b = lst->arr[acrd].item;
		lst->arr[acrd].next = before;
		lst->arr[acrd].prev = lst->arr[before].prev;
		if(EMPTY_NUM != lst->arr[before].prev)
			lst->arr[lst->arr[before].prev].next = acrd;
		else
			lst->idx_first = acrd;
		lst->arr[before].prev = acrd;		
		return acrd;
	}
	return EMPTY_NUM;
}

void list_take_at(volatile t_list *lst, uint8_t num) {
	if(lst->idx_first == num)
		lst->idx_first = lst->arr[num].next;
	if(lst->idx_last == num)
		lst->idx_last = lst->arr[num].prev;
	if(EMPTY_NUM != lst->arr[num].prev)
		lst->arr[lst->arr[num].prev].next = lst->arr[num].next;
	if(EMPTY_NUM != lst->arr[num].next)
		lst->arr[lst->arr[num].next].prev = lst->arr[num].prev;
	//
    lst->arr[num].next = lst->heap;
    lst->heap = num;
}