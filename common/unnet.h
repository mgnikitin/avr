/**
  * version 0.6
  * 26.03.2015
  */

#include "config.h"
#include "types.h"
#include "queuebuf.h"
#include "task_mngr.h"

typedef enum {
	ST_IDLE           = 0x00,	///< \brief линия свободна
	ST_INIT_READING   = 0x01,	///< \brief инициализация чтения
	ST_SYNC_READING   = 0x02,	///< \brief синхронизация чтения бита
	ST_READING        = 0x03,	///< \brief чтение бита
	ST_READ_MASK      = 0x03,
	ST_INIT_SENDING   = 0x04,	///< \brief инициализация записи
	ST_SEND_SYNC      = 0x08,	///< \brief синхронизация записи бита
	ST_SEND_ONE_BIT   = 0x0C,	///< \brief запись логической 1
	ST_SEND_ZERO_BIT  =	0x10,	///< \brief запись логического 0
	ST_WRITE_MASK     = 0x1C,
	ST_MASK 		  = 0x1F
} DEVICE_STATE;

#define UNET_PIN_MASK 		0x80
#define UNET_WR_PIN_MASK 	0x40
#define DATA_FLD_SZ 10				// размер поля Дата пакета

// ----------------------------------------------------------------------------
// task specific
#ifndef TSK_UNET_DEQUEUE
#error "TSK_UNET_DEQUEUE must be defined"
#endif
#ifndef TSK_UNET_RECEIVE
#error "TSK_UNET_RECEIVE must be defined"
#endif
#ifndef TSK_UNET_HANDLE
#error "TSK_UNET_HANDLE must be defined"
#endif
// ----------------------------------------------------------------------------

typedef enum
{
	C_NONE = 0x00,
	C_PING_RQ = 0x01,		// запрос наличия устройства
	C_PING_RP = 0x02,		// ответ на команду C_PING_RQ
	C_ACCPT = 0x03,			// ответ на команду (сигнализировать о том, что команда принята)
	C_SWON_L = 0x04,		// включить освещение
	C_SWOFF_L = 0x05,		// выключить освещения
	C_ALM_W = 0x10,			// датчик протечек тревога
	C_HMDTY = 0x11,			// показания датчика влажности
	C_DATA = 0xF0			// данные
} COMMAND_TYPE;

typedef enum {
	RV_NONE,
	RV_ADDR_DEST,		///< \brief прием адреса назначения
	RV_ADDR_SRC,		///< \brief прием адреса отправителя
	RV_CMD,				///< \brief прием команды
	RV_SIZE,			///< \brief прием размера поля данных
	RV_DATA,			///< \brief прием данных
	RV_CRC,
	RV_FINISHED
} RECEIVE_STATE;

typedef struct {
	uint8_t addr_dst;		///< \brief адрес назначения
	uint8_t addr_src;		///< \brief адрес источника (отправителя)
	uint8_t cmd;			///< \brief тип команды
	uint8_t crc;
	uint8_t sz;				///< \brief размер поля Дата (для команды C_DATA)
	uint8_t dat[DATA_FLD_SZ];	///< \brief указатель на массив данных (поле Дата, команда C_DATA)
	uint8_t rcv_st;			///< состояние приема (чему соответствует принимаемый байт)
	uint8_t rcv_crc;
	
} t_unet_pckt;

typedef void (*unet_hanldr_t)(const t_unet_pckt *);

#define UNET_BROADCAST_ADDR 0xFF	// широковещательный адрес

#ifndef swon_unet_led_macro
#define swon_unet_led_macro
#endif
#ifndef swoff_unet_led_macro
#define swoff_unet_led_macro
#endif

#ifndef swon_unet_led
#define swon_unet_led
#endif
#ifndef swoff_unet_led
#define swoff_unet_led
#endif

#define UNET_DDR_IN		UNET_DDR &= ~_BV(UNET_PIN)
#define UNET_DDR_OUT	UNET_DDR |= _BV(UNET_PIN)

// переходим к чтению
#define turn_read_macro	UNET_DDR_IN;

// переходим к записи
#define turn_write_macro UNET_DDR_OUT;

#define UNET_REG GPIOR0
#define UNET_SMP_COUNT GPIOR1

#define inc_sample_cnt_macro	\
	++UNET_SMP_COUNT;

extern uint8_t unet_crc_pckt(const t_unet_pckt *p);

extern void receive_func(void);

extern void unet_deque_task(void);

BOOL unet_init(unet_hanldr_t recv_hanldr);

extern void unet_timer_handle(void);

extern BOOL unet_sendb(uint8_t addr, uint8_t b);

// отправить сообщение
extern BOOL unet_send(uint8_t addr, uint8_t *arr, uint8_t sz);

// поместить сообщение в стек
extern BOOL unet_enque(const t_unet_pckt *ppckt);