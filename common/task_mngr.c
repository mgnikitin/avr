#include "task_mngr.h"

t_task arr_sheduler[TASK_COUNT];

//volatile uint8_t curr_num = 0;

void TimerServiceTask(void);

//
void Idle(void) {

}

/** ������� ��������� ������ �� �������.
  */
void SetTimerTask(t_task *pt, uint16_t new_time)
{
//	uint8_t sreg = STATUS_REG;
	
//	Disable_Interrupt			// ��������� ����������
	pt->time = new_time;

//	STATUS_REG = sreg;		// ���������� ����������
}

void SetTask(t_task *pt) {
//	uint8_t sreg = STATUS_REG;
//	Disable_Interrupt
	pt->active = 1;
//	STATUS_REG = sreg;		// ���������� ����������
}

void SetTask_isr(t_task *pt) {
	pt->active = 1;
}
/*
void SetTask_isr(t_task *pt)
{
	t_task *ptask = shdlr.acqrd;
	t_task *prev = shdlr.acqrd;
	
	if(shdlr.acqrd == pt)
		return;
	if(shdlr.acqrd->pr < pt->pr) {
		pt->next = shdlr.acqrd;
		shdlr.acqrd = pt;
		return;
	}
	
	ptask = shdlr.acqrd->next;
	prev = shdlr.acqrd;
	while(ptask) {
		if(ptask == pt)
			return;
		if(ptask->pr < pt->pr) {
			pt->next = ptask;
			prev->next = pt;
		}
		prev = ptask;
		ptask = ptask->next;
	}
}
*/

void InitRTOS(void)
{
	uint8_t i;

	CURR_TASK_NUM = 0;

	//shdlr.arr = arr_sheduler;
	//shdlr.acqrd = (shdlr.arr + TSK_IDLE);

	for(i = 0; i < TASK_COUNT; i++)
	{
		arr_sheduler[i].hdlr = Idle;
		arr_sheduler[i].time = 0;
		arr_sheduler[i].active = 0;
	}
	
	tm_reg_task(TSK_TR_SRV, TimerServiceTask);
}

void tm_reg_task(uint8_t id, task_func func) {
	arr_sheduler[id].hdlr = func;
	arr_sheduler[id].time = 0;
	arr_sheduler[id].active = 0;
}

void tm_exec(void) {

	while(1) 		// ������� ���� ����������
	{
		// wdt_reset();	

		if(arr_sheduler[CURR_TASK_NUM].active) {
			Disable_Interrupt
			arr_sheduler[CURR_TASK_NUM].active = 0;
			Enable_Interrupt
			arr_sheduler[CURR_TASK_NUM].hdlr();
		}
		else {
			asm volatile (
			"nop \n\t"
			"nop \n\t"
			"nop \n\t"
			"nop \n\t"
			"nop \n\t"
			);
		}
		
		Disable_Interrupt
		CURR_TASK_NUM = (CURR_TASK_NUM + 1) & (TASK_COUNT - 1);
		Enable_Interrupt
	}
}

void TimerServiceTask(void)
{
	uint8_t i;

	for(i = 0; i < TASK_COUNT; i++) {
		if(0 != arr_sheduler[i].time) {
			if(1 == arr_sheduler[i].time)
				SetTask(&arr_sheduler[i]);
			--arr_sheduler[i].time;
		}
	}
}