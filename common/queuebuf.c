/**
  * version 0.3
  * 15.03.2015
  */
#include "queuebuf.h"

// ------------------------------------------------------------------
void init_que(volatile t_queue *pq,  uint8_t *arr, uint8_t sz) {
    pq->offset_set = 0;
    pq->offset_get = 0;
    pq->_count = 0;
    pq->sz = sz;
    pq->arr = arr;
}

// ------------------------------------------------------------------
BOOL enque(volatile t_queue *pq, uint8_t b) {
    if(pq->_count < pq->sz)
    {
        pq->arr[pq->offset_set] = b;
        pq->offset_set = ++pq->offset_set % pq->sz;
        pq->_count++;
        return TRUE;
    }
    return FALSE;
}

// ------------------------------------------------------------------
// принудительная запись в очередь
void enque_f(volatile t_queue *pq, uint8_t b) {
	if(pq->_count >= pq->sz) {
		pq->offset_get = ++pq->offset_get % pq->sz;
	}
	else
		pq->_count++;
	pq->arr[pq->offset_set] = b;
	pq->offset_set = ++pq->offset_set % pq->sz;
}

// ------------------------------------------------------------------
BOOL acque(volatile t_queue *pq, volatile uint8_t *b) {
	if(pq->_count < pq->sz)
    {
		*b = pq->arr[pq->offset_set];
		pq->offset_set = ++pq->offset_set % pq->sz;
        ++pq->_count;
		return TRUE;
	}
	return FALSE;
}

// ------------------------------------------------------------------
BOOL deque(volatile t_queue *pq, volatile uint8_t *b) {
    if(pq->_count > 0)
    {
        *b = pq->arr[pq->offset_get];
        pq->offset_get = ++pq->offset_get % pq->sz;
        pq->_count--;
        return TRUE;
    }
    return FALSE;
}

// ------------------------------------------------------------------
BOOL deque_n(volatile t_queue *pq) {
    if(pq->_count > 0)
    {
        pq->offset_get = ++pq->offset_get % pq->sz;
        pq->_count--;
        return TRUE;
    }
    return FALSE;
}

// ------------------------------------------------------------------
void copy_attr(volatile t_queue *trg, volatile const t_queue *src)
{
    trg->offset_get = src->offset_get;
    trg->offset_set = src->offset_set;
    trg->_count = src->_count;
    trg->sz = src->sz;
    trg->arr = src->arr;
}

// ------------------------------------------------------------------
BOOL qfind(volatile t_queue *pq, const uint8_t *arr, uint8_t size) {
    uint8_t b;
    // результат
    BOOL rez = FALSE;
    // номер элемента для сравнения
    uint8_t pos_cmp = 0;
    // сохраненное состояние
    volatile t_queue svd_pq;
    volatile t_queue rez_pq;
    svd_pq._count = 0;	// флаг сохраненных аттрибутов
    copy_attr(&rez_pq, pq);

    if(size == 0)
        return rez;

    while(deque(pq, &b))
    {
        // сравниваем байты буфера с массивом
        if(b == arr[pos_cmp])
        {
            pos_cmp++;
        }
        else
        { // байты не совпали начинаем поиск сначала
            pos_cmp = 0;

            // пытаемся восстановить сохраненные аттрибуты
            if(svd_pq._count)
                copy_attr(pq, &svd_pq);

            // обновляем атрибуты
            copy_attr(&rez_pq, pq);

            // сохраненные атрибуты больше не нужны, повторяем попытку
            if(svd_pq._count)
            {
                svd_pq._count = 0;
                continue;
            }
        }

        // если массивы совпали, восстанавливаем атрибуты и выходим
        if(pos_cmp >= size)
        {
            copy_attr(pq, &rez_pq);
            rez = TRUE;
            break;
        }

        // сохраняем состояние атрибутов
        if(pos_cmp == 1)
            copy_attr(&svd_pq, pq);
    }

    return rez;
}

// ------------------------------------------------------------------
BOOL qfind_silent(volatile t_queue *pq, const uint8_t *arr, uint8_t size)
{
    volatile t_queue svd_pq;
    copy_attr(&svd_pq, pq);
    BOOL rez = qfind(pq, arr ,size);
    copy_attr(pq, &svd_pq);
    return rez;
}
