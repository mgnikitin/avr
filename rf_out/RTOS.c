/*
 * RTOS.c
 *
 * Created: 11.03.2014 22:11:35
 *  Author: Asus
 */ 

#include "env.h"
#include "queuebuf.h"
#include "routing.h"
#include "asmroutine.h"
// Port B
#define RF_PORT PORTB
#define RF_PORT_RO PINB
#define RF_DDR DDRB
#define RF_PIN 1
// Port C
#define LED_DDR DDRC
#define LED_PORT PORTC
#define LED_PORT_RO PINC
#define STATUS_LED 3
#define UNET_PORT PORTC
#define UNET_PORT_RO PINC
#define UNET_DDR DDRC
#define UNET_PIN 0
// Port D

//

#define LED_FAIL STATUS_LED
#define UART_BUFFER_SIZE 20
#define UNET_INTERVAL_MS 1
#define UNET_ADDR 0x05

#define SwonLED(led) LED_PORT |= (1 << led)	 	/* логичеcкая единица  */
#define SwoffLED(led) LED_PORT &= ~(1 << led)	/* логический ноль */
#define IsLED(led) (LED_PORT_RO & (1 << led))


// массив байт UART на отправку
static uint8_t arr_uart_out[UART_BUFFER_SIZE];

volatile t_queue q_uart_out;
//static volatile t_queue q_uart_in;

static volatile uint8_t b_uart_out;

volatile uint16_t t0_compa_cnt = 0;
volatile uint16_t t1_compa_cnt = 0;

volatile uint32_t mm[2] = {0, 0};
volatile uint32_t d;
volatile uint16_t t;
volatile uint8_t  t0_compa_upd = 0;
volatile uint16_t t1_compa_upd = 0;
volatile uint8_t  int0_upd = 0;

#define uart_log_isr_macro(b) 		\
	if(enque(&q_uart_out, b))			\
			Enable_UART_Int(UDRIE_MASK);

// Прерывание таймера 1
//ISR(TIMER1_COMPA_vect) {
	//t1_compa_upd = 1;
//}

/* USART Rx Complete */
/*ISR(USART_RX_vect) {
	// принудительно записываем в буфер,
	// если в буфере нет места. первый элемент
	// будет удален
	enque_f(&q_uart_in, UDR);
}*/

/* USART, Data Register Empty */
ISR(USART_UDRE_vect) {
	if(deque(&q_uart_out, &b_uart_out))
		UDR = b_uart_out;
	else	// очередь пуста, запрещаем прерывания по опустошению буфера передачи UDR UART
		Disable_UART_Int(UDRIE_MASK);
}

/* USART Tx Complete */
ISR(USART_TX_vect) {
	
}

void uart_log(uint8_t b) {
	safe_Disable_Interrupt

	if(enque(&q_uart_out, b))
	{
		Enable_UART_Int(UDRIE_MASK);
	}
	safe_Enable_Interrupt
}

// ------------------------------------------------------------------
inline INLINE void InitAll(void)
{
	// init buffers
	init_que(&q_uart_out, arr_uart_out, UART_BUFFER_SIZE);
	//init_que(&q_uart_in, arr_uart_in, UART_BUFFER_SIZE);
	//InitUSART
	// set baudrate
	UBRRL = LO(bauddivider);
	UBRRH = HI(bauddivider);
	// 
	UCSRA = 0;
	// enable receiver and transmiter
	UCSRB = 1<<RXEN|1<<TXEN;
	Disable_UART_Int(TXCIE_MASK|UDRIE_MASK);
//	Enable_UART_Int(RXCIE_MASK);
	UCSRC = /*1<<URSEL|*/1<<UCSZ0|1<<UCSZ1;
	// Timer 0
	TCCR0A = 1<<WGM01;				// CTC Mode - Автосброс после достижения регистра сравнения
	TCCR0B = (0<<CS00) | (1<<CS01);				// Prescaler: Freq = no -
	TCNT0 = 0;						// Установить начальное значение счётчиков
	OCR0A  = F_CPU/ 8L /20000L; 		// Установить значение в регистр сравнения
	TIMSK0 = 1<<OCIE0A;			// Разрешаем прерывание RTOS - запуск ОС
	// Timer 1
	TCCR1A = 0;
	TCCR1B = 1 << WGM12;		// CTC Mode
	TCCR1B |= (1 << CS10);		// Prescaler:  no
	OCR1A =  (F_CPU/ 1000L);		// write 16-bit register
	//TIMSK1 = 1<<OCIE1A;
	// external interrupt
	//EICRA = (0 << ISC01) | (1 << ISC00);
	//EIMSK = (1 << INT0);
	RF_DDR |= (1 << RF_PIN);
	// led
	LED_DDR |= 1 << STATUS_LED;		// 
	//

}

int main(void)
{	
	//uint8_t d[2] = {0,0 };
	//uint8_t dd;
	InitAll();
	Enable_Interrupt
	
	while(1)
	{
		if(t0_compa_upd)
		{
			//++t0_compa_cnt;
			//d[0] = d[1];
			//d[1] = t0_compa_upd;
			//dd = d[1] - d[0];
			if(RF_PORT_RO & RF_PIN)
			{
				RF_PORT &= ~(1 << RF_PIN);
				SwoffLED(STATUS_LED);
			}
			else
			{
				RF_PORT |= (1 << RF_PIN);
				SwonLED(STATUS_LED);
			}
			//if(t0_compa_cnt % 10000 == 0)
			//{
			//	uart_log(dd);
			//}
			t0_compa_upd = 0;
		}
	}
	return 0;
}