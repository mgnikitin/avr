#ifndef _ENV_
#define _ENV_


#include <avr/io.h>
#ifndef __ASSEMBLER__
#include <avr/interrupt.h>
//#include <avrlibtypes.h>
//#include <avrlibdefs.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#endif

#define STATUS_REG 			SREG
#define Interrupt_Flag		SREG_I
#define Disable_Interrupt	cli();
#define Enable_Interrupt	sei();
#define Is_Interrupt 		(STATUS_REG & (1<<Interrupt_Flag))

#define INLINE __attribute__((always_inline))

#ifndef __ASSEMBLER__
typedef int BOOL;
typedef unsigned char uint8_t;
#endif

#define TRUE 1
#define FALSE 0

// ----- UART -----
#define UDR UDR0
#define UBRR UBRR0
#define UBRRL UBRR0L
#define UBRRH UBRR0H
#define UCSRA UCSR0A
#define UCSRB UCSR0B
#define UCSRC UCSR0C
#define TXEN TXEN0
#define RXEN RXEN0
#define UDRE UDRE0
#define UDRIE UDRIE0
#define TXCIE TXCIE0
#define RXCIE RXCIE0
#define UCSZ0 UCSZ00
#define UCSZ1 UCSZ01

#define UDRIE_MASK (1<<UDRIE)
#define RXCIE_MASK (1<<RXCIE)
#define TXCIE_MASK (1<<TXCIE)

#define Enable_UART_Int(flags) UCSRB |= flags
#define Disable_UART_Int(flags) UCSRB &= ~(flags)
// ----------------

#define RTOS_ISR  			TIMER0_COMPA_vect 

//Clock Config
#define F_CPU 16000000L
#define F_CPU_MHz 16

//System Timer Config
#define Prescaler	  	64
#define	TimerDivider  		(F_CPU/Prescaler/1000)		// 1 mS

#define baudrate 9600L
#define bauddivider ((F_CPU+baudrate*8)/(baudrate*16)-1)

#define HI(x) ((x)>>8)
#define LO(x) ((x)& 0xFF)

#define TASK_COUNT 10
#define TIMER_COUNT 10

#ifndef __ASSEMBLER__
extern volatile uint16_t t1_compa_cnt;

enum STATUS_RTOS
{
	TASK_QUEUE_OK,
	TASK_QUEUE_OVERFLOW
};
#endif

#endif  // _ENV_