#ifndef __DEF_REMOTE_RECEIVER__
#define __DEF_REMOTE_RECEIVER__

#include "env.h"
#include "routing.h"
#include "queuebuf.h"

extern volatile t_queue q_uart_out;

#define uart_log_isr_macro(b) 		\
	if(enque(&q_uart_out, b))			\
	{									\
		if(!(UCSRB & UDRIE_MASK))			\
			Enable_UART_Int(UDRIE_MASK);	\
	}

typedef void (*RemoteReceiverCallBack)(unsigned long, unsigned int);

extern void receiver_isr(void);

extern void receiver_init(uint8_t pn, 
				    volatile uint8_t *prt, 
				    volatile uint8_t *prt_ro,
				    volatile uint8_t *prt_dir,
					uint8_t minRepeats,
				    RemoteReceiverCallBack callback);

#endif /* __DEF_REMOTE_RECEIVER__ */