#include "remote_receiver.h"

static volatile uint8_t *pport = &PORTB;	// GPIO-порт
static volatile uint8_t *pport_ro = &PINB;	// GPIO-порт на чтение
static volatile uint8_t *pddr = &DDRB;		// направление GPIO-порта
static uint8_t pin;					// бит GPIO-порта

#define B1110 14
#define B1111 15
#define B11   3
#define B1    1
#define B0101 5
#define B1010 10
#define B0110 6

volatile int8_t _state = -1;
BOOL _inCallback = FALSE;
RemoteReceiverCallBack _callback;
uint8_t _minRepeats;

void receiver_isr() {
	static unsigned int period;				// Calculated duration of 1 period
	static uint8_t receivedBit;				// Contains "bit" currently receiving
	static unsigned long receivedCode;		// Contains received code
	static unsigned long previousCode;		// Contains previous received code
	static uint8_t repeats = 0;				// The number of times the an identical code is received in a row.
	static uint32_t edgeTimeStamp[3] = {0, };
	static unsigned int min1Period, max1Period, min3Period, max3Period;
	static BOOL skip = FALSE;
	
	edgeTimeStamp[1] = edgeTimeStamp[2];
	edgeTimeStamp[2] = microsec(); //TODO microsec_isr_macro();
	
	if(skip) 
	{
		skip = FALSE;
		return;
	}
	
	if (_state >= 0 && edgeTimeStamp[2]-edgeTimeStamp[1] < min1Period) {
		// Last edge was too short.
		// Skip this edge, and the next too.		
		skip = TRUE;
		uart_log_isr_macro('s');
		return;
	}
	uart_log_isr_macro('i')
	unsigned int duration = edgeTimeStamp[1] - edgeTimeStamp[0];
	edgeTimeStamp[0] = edgeTimeStamp[1];
	if (_state==-1) { // Waiting for sync-signal
		if (duration>3720) { // =31*120 minimal time between two edges before decoding starts.
			// Sync signal received.. Preparing for decoding
			period=duration/31;
			receivedCode=previousCode=repeats=0;

			// Allow for large error-margin. ElCheapo-hardware :(
			min1Period=period*4/10; // Avoid floating point math; saves memory.
			max1Period=period*16/10;
			min3Period=period*23/10;
			max3Period=period*37/10;
			uart_log_isr_macro('d');
		}
		else {
			//uart_log_isr_macro('q');
			return;
		}
	} else if (_state<48) { // Decoding message
		receivedBit <<= 1;

		// bit part durations can ONLY be 1 or 3 periods.
		if (duration<=max1Period) {
			receivedBit &= B1110; // Clear LSB of receivedBit
		}
		else if (duration>=min3Period && duration<=max3Period) {
			receivedBit |= B1; // Set LSB of receivedBit
		}
		else { // Otherwise the entire sequence is invalid
			uart_log_isr_macro('s');
			//uint8_t d = 0xFF & duration;
			//uint8_t d1 = d / 10 - (d / 100) * 10;
			//uart_log_isr_macro(0x30 + d / 100);
			//uart_log_isr_macro(0x30 + d1*10);
			//uart_log_isr_macro(0x30 + d - d / 100 - d1 * 10);
			_state=-1;
			return;
		}

		if ((_state%4)==3) { // Last bit part?
			// Shift
			receivedCode*=3;

			// Only 4 LSB's are used; trim the rest.
			switch (receivedBit & B1111) {
				case B0101: // short long short long == B0101
					// bit "0" received
					receivedCode+=0; // I hope the optimizer handles this ;)
					break;
				case B1010: // long short long short == B1010
					// bit "1" received
					receivedCode+=1;
					break;
				case B0110: // short long long short
					// bit "f" received
					receivedCode+=2;
					break;
				default:
					// Bit was rubbish. Abort.
					_state=-1;
					uart_log_isr_macro('r');
					return;
			}
		}
	} else if (_state==48) { // Waiting for sync bit part 1
		// Must be 1 period.
		uart_log_isr_macro('l');
		if (duration>max1Period) {
			_state=-1;
			return;
		}
	} else { // Waiting for sync bit part 2
		// Must be 31 periods.
		uart_log_isr_macro('w');
		if (duration<period*25 || duration>period*36) {
		  _state=-1;
		  return;
		}

		// receivedCode is a valid code!

		if (receivedCode!=previousCode) {
			repeats=0;
			previousCode=receivedCode;
		}

		repeats++;

		if (repeats>=_minRepeats) {
			if (!_inCallback) {
				_inCallback = TRUE;
				(_callback)(receivedCode, period);
				_inCallback = FALSE;
			}
			// Reset after callback.
			_state=-1;
			return;
		}

		// Reset for next round
		receivedCode = 0;
		_state=0; // no need to wait for another sync-bit!
		return;
	}

	_state++;
}

void receiver_init(uint8_t pn, 
				   volatile uint8_t *prt, 
				   volatile uint8_t *prt_ro,
				   volatile uint8_t *prt_dir,
				   uint8_t minRepeats,
				   RemoteReceiverCallBack callback
  				) {
	pin = pn;
	pport = prt;
	pport_ro = prt_ro;
	pddr = prt_dir;
	_minRepeats = minRepeats;
	_callback = callback;
}