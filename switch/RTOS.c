/*
 * RTOS.c
 *
 * Created: 11.03.2014 22:11:35
 *  Author: Asus
 */ 

#include "task_mngr.h"
#include "unnet.h"
#include "routing.h"
#include "virtual_wire.h"

#define LED_FAIL STATUS_LED
#define UART_BUFFER_SIZE 16	// use &

#define SwonLED(led) LED_PORT |= (1 << led)	 	/* логичеcкая единица  */
#define SwoffLED(led) LED_PORT &= ~(1 << led)	/* логический ноль */
#define IsLED(led) (LED_PORT_RO & (1 << led))

void DiodeOffTask(void);
void SwitchOn(void);
void SwitchOff(void);

// массив байт UART на отправку
static uint8_t arr_uart_out[UART_BUFFER_SIZE];
// массив байт, считанных с UART
static uint8_t arr_uart_in[UART_BUFFER_SIZE];

static volatile t_que q_uart_out;
static volatile t_que q_uart_in;

static volatile uint8_t b_uart_out;

void swon_unet_led(void) {
	SwonLED(STATUS_LED);
}

void swoff_unet_led(void) {
	SwoffLED(STATUS_LED);
}


//RTOS Interrupt
ISR(RTOS_ISR)
{
	TASK_PTR(TSK_TR_SRV)->active = 1;
}

ISR(TIMER2_COMPA_vect) {
	unet_pin_st = UNET_PORT_RO & _BV(UNET_PIN);
	TASK_PTR(TSK_UNET_HANDLE)->active = 1;
	curr_num = 0;
	//SetTask_isr(TASK_PTR(TSK_UNET_HANDLE));
}

/* USART Rx Complete */
ISR(USART_RX_vect) {
	// принудительно записываем в буфер,
	// если в буфере нет места. первый элемент
	// будет удален
	q_uart_in.arr[q_uart_in.offset_set] = UDR;
	q_uart_in.offset_set = (q_uart_in.offset_set + 1) & (UART_BUFFER_SIZE - 1);
}

/* USART, Data Register Empty */
ISR(USART_UDRE_vect) {
	UDR = q_uart_out.arr[q_uart_out.offset_get];
	q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);
	// очередь пуста, запрещаем прерывания по опустошению буфера передачи UDR UART
	if(q_uart_out.offset_get == q_uart_out.offset_set)
		Disable_UART_Int(UDRIE_MASK);
}

/* USART Tx Complete */
ISR(USART_TX_vect) {
	
}

void uart_log(uint8_t b) {
	uint8_t sreg = STATUS_REG;
	Disable_Interrupt
	
	q_uart_out.arr[q_uart_out.offset_set] = b;
	q_uart_out.offset_set = (q_uart_out.offset_set + 1) & (UART_BUFFER_SIZE - 1);
	if(q_uart_out.offset_set == q_uart_out.offset_get)
		q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);

	STATUS_REG = sreg;
	Enable_UART_Int(UDRIE_MASK);
}

void write_to_uart_buf(const uint8_t *arr, uint8_t sz) {
	uint8_t i;
	for(i = 0; i < sz; i++)
	{
		uart_log(arr[i]);
	}
}

void unet_handlr(const t_unet_pckt *ppckt) {
	t_unet_pckt pckt;
	
	switch(ppckt->cmd) {
	case C_PING_RQ:			// запрос пинг
		uart_log('P');
		// формируем ответное сообщение
		pckt.addr_dst = ppckt->addr_src;
		pckt.addr_src = UNET_ADDR;
		pckt.cmd = C_PING_RP;
		if(!unet_send(ppckt->addr_src, &pckt.cmd, 1))
		{
			if(!unet_enque(&pckt))
			{
				uart_log('F');
				// TODO
			}
			else
				uart_log('q');
		}
		else
			uart_log('p');
	break;
	case C_PING_RP:			// ответ пинг
		uart_log('R');
	break;
	case C_ACCPT:
		// TODO
	break;
	case C_SWON_L:
		uart_log('O');
		SwitchOn();
		pckt.addr_dst = ppckt->addr_src;
		pckt.addr_src = UNET_ADDR;
		pckt.cmd = C_ACCPT;
		if(!unet_send(ppckt->addr_src, &pckt.cmd, 1))
		{
			if(!unet_enque(&pckt))
			{
				// TODO
				uart_log('f');
			}
			else
				uart_log('q');
		}
		else
			uart_log('p');
	break;
	case C_SWOFF_L:
		uart_log('L');
		SwitchOff();
		pckt.addr_dst = ppckt->addr_src;
		pckt.addr_src = UNET_ADDR;
		pckt.cmd = C_ACCPT;
		if(!unet_send(ppckt->addr_src, &pckt.cmd, 1))
		{
			if(!unet_enque(&pckt))
			{
				// TODO
				uart_log('f');
			}
			else
				uart_log('q');
		}
		else
			uart_log('p');
	break;
	case C_ALM_W:			// датчик протечек тревога

	break;
	case C_DATA:
	break;
	case C_NONE:
	default:
		uart_log('u');
	break;
	}
}

void DiodeOnTask(void) {
	SwonLED(STATUS_LED);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOffTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}

void DiodeOffTask(void) {
	SwoffLED(STATUS_LED);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOnTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}

void SwitchOn(void) {
	SWITCH_PORT |= 1 << SWITCH_PIN;
}

void SwitchOff(void) {
	SWITCH_PORT &= ~(1 << SWITCH_PIN);
}

/*
void PingTask(void) {
	uint8_t b = C_PING_RQ;
	if(!unet_send(0x02, &b, 1)) {
		uart_log('f');
	}
	tt.task = PingTask;
	SetTimerTask(tt, 10000);
}

void DevicePresentsTask(void) {
	t_unet_pckt pckt;
	pckt.addr_dst = UNET_BROADCAST_ADDR;
	pckt.addr_src = UNET_ADDR;
	pckt.cmd = C_PING_RP;
	if(!unet_send(pckt.addr_dst, &pckt.cmd, 1)) {
		if(!unet_enque(&pckt)) {
			tt.task = DevicePresentsTask;
			SetTimerTask(tt, 10000);
		}
	}
}
*/

// ------------------------------------------------------------------
void VirtualWire_handle(const uint8_t* buf, uint8_t len) {
	if(4 == len)
	{
		if(('U' == buf[0]) && ((UNET_ADDR == buf[1]) || (UNET_BROADCAST_ADDR == buf[1])))
		{
			switch(buf[3])
			{
				case C_SWON_L:
					uart_log('O');
					if(IsLED(STATUS_LED))
						SwoffLED(STATUS_LED);
					else
						SwonLED(STATUS_LED);
					SwitchOn();
				break;
				case C_SWOFF_L:
					uart_log('L');
					if(IsLED(STATUS_LED))
						SwoffLED(STATUS_LED);
					else
						SwonLED(STATUS_LED);
					SwitchOff();
				break;
				default:
				break;
			}
		}
	}

}

// ------------------------------------------------------------------
inline INLINE void InitAll(void)
{
	t_vw_setup vw_prm;
	// init buffers
	q_uart_out.arr = arr_uart_out;
	q_uart_out.offset_set = 0;
    q_uart_out.offset_get = 0;
	q_uart_in.arr = arr_uart_in;
	q_uart_in.offset_set = 0;
    q_uart_in.offset_get = 0;
	//InitUSART
	// set baudrate
	UBRRL = LO(bauddivider);
	UBRRH = HI(bauddivider);
	// 
	UCSRA = 0;
	// enable receiver and transmiter
	UCSRB = 1<<RXEN|1<<TXEN;
	Disable_UART_Int(TXCIE_MASK|UDRIE_MASK);
	Enable_UART_Int(RXCIE_MASK);
	UCSRC = /*1<<URSEL|*/1<<UCSZ0|1<<UCSZ1;
	// led
	LED_DDR |= 1 << STATUS_LED;		// 
	//
	SWITCH_DDR |= 1 << SWITCH_PIN;
	// virtual wire
	vw_prm.pddr_rx = &VW_RX_DDR;
	vw_prm.pddr_tx = &VW_TX_DDR;
	vw_prm.ready_rx_handle = VirtualWire_handle;
    //vw_setup(2000, &vw_prm);      // Bits per sec
    //vw_rx_start();       // Start the receiver PLL running
	// Timer 0
	TCCR0A = 1<<WGM01;				// CTC Mode Автосброс после достижения регистра сравнения
	TCCR0B = TCCR0B_PRESCALER;		// Prescaler:
	TCNT0 = 0;						// Установить начальное значение счётчиков
	OCR0A  = TimerDivider; 		// Установить значение в регистр сравнения
	TIMSK0 |= 1<<OCIE0A;			// Разрешаем прерывание RTOS - запуск ОС
	// Timer 2
	TCCR2A = _BV(WGM21);			// CTC Mode
	TCCR2B = TCCR2B_PRESC_8;		// Prescaler
	TCNT2  = 0;
	OCR2A  = F_CPU/8/10000L;
	TIMSK2 |= _BV(OCIE2A);
	// UnNet task
	if(!unet_init(unet_handlr))
		SwonLED(LED_FAIL);
}

// ------------------------------------------------------------------
int main(void)
{
	InitAll();
	InitRTOS();			//

	tm_reg_task(TSK_DIOD, DiodeOnTask);
	tm_reg_task(TSK_UNET_DEQUEUE, unet_deque_task);
	tm_reg_task(TSK_UNET_RECEIVE, receive_func);
	tm_reg_task(TSK_UNET_HANDLE, unet_timer_handle);
	tm_reg_task(TSK_VW_PLL, vw_pll);
	
	// Diod task
	SetTimerTask(TASK_PTR(TSK_DIOD), (uint16_t)1000);

	// present unet
	//tt.task = DevicePresentsTask;
	//SetTimerTask(tt, 1000);
	// ping test
	//tt.task = PingTask;
	//SetTimerTask(tt, 10000);
	// switch test
	//tt.task = SwitchOn;
	//SetTimerTask(tt, 2000);
	//
	//tt.task = SendTimeTask;
	//SetTimerTask(tt, 4000);	
	
	tm_exec();
	return 0;
}