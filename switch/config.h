
#define UNET_ADDR 0x05

// Port B

// Port C
#define LED_DDR DDRC
#define LED_PORT PORTC
#define LED_PORT_RO PINC
#define STATUS_LED 5
#define UNET_PORT PORTC
#define UNET_PORT_RO PINC
#define UNET_DDR DDRC
#define UNET_PIN 0
// Port D
#define SWITCH_DDR DDRD
#define SWITCH_PORT PORTD
#define SWITCH_PIN 3
#define VW_RX_DDR DDRD
//#define RF_RCV_PORT PORTD
#define VW_PORT_RX_RO PIND
#define VW_PORT_TX PINB
#define VW_TX_PIN 1
#define VW_RX_PIN 2
#define VW_TX_DDR DDRB
//

// 0 prohibited
#define TSK_UNET_DEQUEUE 3
#define TSK_UNET_RECEIVE 4
#define TSK_UNET_HANDLE  5
#define TSK_VW_PLL		6
#define TSK_DIOD	  	7