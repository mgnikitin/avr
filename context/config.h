
#define UNET_ADDR 0x02
// Port B
#define VW_PORT_TX PORTB
#define VW_PORT_RX_RO PINB
#define RF_DDR DDRB
#define VW_TX_PIN	1
#define VW_RX_PIN	2
// Port C
#define I2C_PORT	PORTC
#define I2C_DDR		DDRC
#define I2C_SCL		0
#define I2C_SDA		1
#define LED_DDR 	DDRC
#define LED_PORT 	PORTC
#define LED_PORT_RO PINC
#define STATUS_LED 	3
#define UNET_PORT 	PORTC
#define UNET_PORT_RO PINC
#define UNET_DDR 	DDRC
#define UNET_PIN 	2
// Port D


// 0, 1 prohibited
#define THREAD_1  	2
#define THREAD_2 	3

