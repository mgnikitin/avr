/*
 * RTOS.c
 *
 * Created: 11.03.2014 22:11:35
 *  Author: Asus
 */ 

#include "config.h"
#include "task_mngr.h"
#include "routing.h"
#include "asmroutine.h"
//

#define LED_FAIL STATUS_LED
//
#define I2C_ADDRESS 0x04 /*0x19*/

#define UART_BUFFER_SIZE 16	// use &

// массив байт UART на отправку
static uint8_t arr_uart_out[UART_BUFFER_SIZE];
// массив байт, считанных с UART
static uint8_t arr_uart_in[UART_BUFFER_SIZE];

static volatile t_que q_uart_out;
static volatile t_que q_uart_in;

static volatile uint8_t b_uart_out;

uint16_t arr_stack1[STACK_SIZE];
uint16_t arr_stack2[STACK_SIZE];

#define SwonLED(led) LED_PORT |= _BV(led)	 	/* логичеcкая единица */
#define SwoffLED(led) LED_PORT &= ~_BV(led)		/* логический ноль */
#define IsLED(led) (LED_PORT_RO & _BV(led))

void swon_unet_led(void) {
	SwonLED(STATUS_LED);
}

void swoff_unet_led(void) {
	SwoffLED(STATUS_LED);
}

extern void uart_log(uint8_t);
extern void uart_log_isr(uint8_t);

//RTOS Interrupt
ISR(RTOS_ISR, ISR_NAKED) {
	asm volatile (
		"push r1				\n\t"
		"push r0				\n\t"
		"in   r0, __SREG__		\n\t"
		"push r0				\n\t"
		"eor  r1, r1			\n\t"
		"push r24				\n\t"
		"push r25				\n\t"
	);

	//SetTask_isr(TASK_PTR(TSK_TR_SRV));
	
	asm volatile (
		"pop  r25				\n\t"
		"pop  r24				\n\t"
		"pop  r0				\n\t"
		"out  __SREG__, r0		\n\t"
		"pop  r0				\n\t"
		"pop  r1				\n\t"
		"reti					\n\t"
	);
}

/*
ISR(TIMER2_COMPA_vect, ISR_NAKED) {
	
	asm volatile (
		"push r1				\n\t"
		"push r0				\n\t"
		"in   r0, __SREG__		\n\t"
		"push r0				\n\t"
		"eor  r1, r1			\n\t"
		"push r24				\n\t"
		"push r25				\n\t"
		"in	  r24, %0			\n\t"
		"andi r24, %1			\n\t"
		"sts  unet_pin_st, r24	\n\t"
		:: "I"(_SFR_IO_ADDR(UNET_PORT_RO)), "I"(_BV(UNET_PIN))
	);
	
	//unet_pin_st = UNET_PORT_RO & _BV(UNET_PIN);
	SetTask_isr(TASK_PTR(TSK_UNET_HANDLE));

	asm volatile (
		"pop  r25				\n\t"
		"pop  r24				\n\t"
		"pop  r0				\n\t"
		"out  __SREG__, r0		\n\t"
		"pop  r0				\n\t"
		"pop  r1				\n\t"
		"reti					\n\t"
	);
}
*/

// Прерывание TWI (I2C)
//ISR(TWI_vect) {
//}

/* USART Rx Complete */
ISR(USART_RX_vect) {
	// принудительно записываем в буфер,
	// если в буфере нет места. первый элемент
	// будет удален
	q_uart_in.arr[q_uart_in.offset_set] = UDR;
	q_uart_in.offset_set = (q_uart_in.offset_set + 1) & (UART_BUFFER_SIZE - 1);
}

/* USART, Data Register Empty */
ISR(USART_UDRE_vect) {
	UDR = q_uart_out.arr[q_uart_out.offset_get];
	q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);
	// очередь пуста, запрещаем прерывания по опустошению буфера передачи UDR UART
	if(q_uart_out.offset_get == q_uart_out.offset_set)
		Disable_UART_Int(UDRIE_MASK);
}

/* USART Tx Complete */
ISR(USART_TX_vect) {
	
}

void write_to_uart_buf(const uint8_t *arr, uint8_t sz) {
	uint8_t i;
	for(i = 0; i < sz; i++)
	{
		uart_log(arr[i]);
	}
}

void uart_log(uint8_t b) {
	uint8_t sreg = STATUS_REG;
	Disable_Interrupt
	
	q_uart_out.arr[q_uart_out.offset_set] = b;
	q_uart_out.offset_set = (q_uart_out.offset_set + 1) & (UART_BUFFER_SIZE - 1);
	
	Enable_UART_Int(UDRIE_MASK);
	STATUS_REG = sreg;
}

void IdleTask(void) {
}



void thread1(void) {
	//uint8_t b;
	//uint8_t sreg;
	
	while(TRUE)
	{/*
		sreg = STATUS_REG;
		Disable_Interrupt
		if(q_uart_in.offset_get != q_uart_in.offset_set)
		{
			b = q_uart_in.arr[q_uart_in.offset_get];
			q_uart_in.offset_get = (q_uart_in.offset_get + 1) & (UART_BUFFER_SIZE - 1);
			STATUS_REG = sreg;	
			prot_parse(&b, 1);
		}
		else {
			STATUS_REG = sreg;
			break;
		}
		*/
		asm volatile (
			"nop \n\t"
			"nop \n\t"
			"nop \n\t"
			"nop \n\t"
			"nop \n\t"
		);
 	}
}

void thread2(void) {
	while(TRUE)
	{
		asm volatile (
			"nop \n\t"
			"nop \n\t"
			"nop \n\t"
			"nop \n\t"
			"nop \n\t"
		);
 	}
}

// ------------------------------------------------------------------
void InitAll(void)
{
	// init buffers
	q_uart_out.arr = arr_uart_out;
	q_uart_out.offset_set = 0;
    q_uart_out.offset_get = 0;
	q_uart_in.arr = arr_uart_in;
	q_uart_in.offset_set = 0;
    q_uart_in.offset_get = 0;
	//InitUSART
	// set baudrate
	UBRRL = LO(bauddivider);
	UBRRH = HI(bauddivider);
	// 
	UCSRA = 0;
	// enable receiver and transmiter
	UCSRB = _BV(RXEN)|_BV(TXEN);
	Disable_UART_Int(TXCIE_MASK|UDRIE_MASK);
	Enable_UART_Int(RXCIE_MASK);
	UCSRC = /*1<<URSEL|*/1<<UCSZ0|1<<UCSZ1;
	// Timer 0
	TCCR0A = 1<<WGM01;				// CTC Mode ( Автосброс после достижения регистра сравнения)
	TCCR0B = TCCR0B_PRESCALER;	    // Prescaler
	TCNT0 = 0;						// Установить начальное значение счётчиков
	OCR0A  = TimerDivider; 			// Установить значение в регистр сравнения
	TIMSK0 |= _BV(OCIE0A);			// Разрешаем прерывание RTOS - запуск ОС
	// Timer 2

	//
	// led
	LED_DDR |= 1 << STATUS_LED;		// 

}

// ------------------------------------------------------------------
int main(void)
{
	InitAll();
	InitRTOS();
	
	tm_reg_thread(THREAD_1, 1, thread1, arr_stack1 + STACK_SIZE - 1);
	tm_reg_thread(THREAD_2, 1, thread2, arr_stack2 + STACK_SIZE - 1);
	
	SetTask(TASK_PTR(THREAD_1));

	Enable_Interrupt				// разрещаем прерывания
	tm_exec();
	return 0;
}