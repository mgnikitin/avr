
#define UNET_ADDR 0x11
// Port B
//#define VW_PORT_TX PORTB
//#define VW_PORT_RX_RO PINB
//#define RF_DDR DDRB
//#define VW_TX_PIN	1
//#define VW_RX_PIN	2
#define UNET_PORT 	PORTB
#define UNET_PORT_RO PINB
#define UNET_DDR 	DDRB
#define UNET_PIN 	4
// Port C
#define I2C_PORT	PORTC
#define I2C_DDR		DDRC
#define I2C_SCL		0
#define I2C_SDA		1
#define LED_DDR 	DDRC
#define LED_PORT 	PORTC
#define LED_PORT_RO PINC
#define STATUS_LED 	3
// Port D


// 0 prohibited
#define TSK_UNET_HANDLE  1
#define TSK_UNET_DEQUEUE 2
#define TSK_UNET_RECEIVE 3
#define TSK_DIOD  		 4
#define TSK_DEV_PRESENT	 5
#define TSK_PROT		 6

//
#define swon_unet_led LED_PORT |= _BV(STATUS_LED)

#define swoff_unet_led LED_PORT &= ~_BV(STATUS_LED)



