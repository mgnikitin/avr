/*
 * RTOS.c
 *
 * Created: 11.03.2014 22:11:35
 *  Author: Asus
 */ 

#include "config.h"
#include "task_mngr.h"
#include "queuebuf.h"
#include "unnet.h"
#include "prot.h"
#include "routing.h"
//#include "virtual_wire.h"
#include "asmroutine.h"
 #include <stdlib.h>
//

#define LED_FAIL STATUS_LED
//
#define I2C_ADDRESS 0x04 /*0x19*/

#define UART_BUFFER_SIZE 32	// use &

// массив байт UART на отправку
static uint8_t arr_uart_out[UART_BUFFER_SIZE];
// массив байт, считанных с UART
static uint8_t arr_uart_in[UART_BUFFER_SIZE];

static volatile t_que q_uart_out;
static volatile t_que q_uart_in;

uint8_t ocr_rate = 10;

#define SENT_SIZE 2
static t_sent sents[SENT_SIZE];
static uint8_t tmp_arr_out[PROT_BUFFER_SIZE];

//static uint8_t rf_buf[VW_MAX_MESSAGE_LEN];
//uint8_t rf_buflen = VW_MAX_MESSAGE_LEN;

#define SwonLED(led) LED_PORT |= _BV(led)	 	/* логичеcкая единица */
#define SwoffLED(led) LED_PORT &= ~_BV(led)		/* логический ноль */
#define IsLED(led) (LED_PORT_RO & _BV(led))

extern void IdleTask(void);
extern void DiodeOffTask(void);
extern void protCollectTask(void);
extern void uart_log(uint8_t);
//extern void uart_log_isr(uint8_t);
extern void unet_timer_handle(void);

//RTOS Interrupt
ISR(RTOS_ISR) {
	TASK_PTR(TSK_TR_SRV)->active = 1;
	//SetTask_isr(TASK_PTR(TSK_TR_SRV));
}

/*
ISR(RTOS_ISR, ISR_NAKED) {
	asm volatile (
		"push r1				\n\t"
		"push r0				\n\t"
		"in   r0, __SREG__		\n\t"
		"push r0				\n\t"
		"eor  r1, r1			\n\t"
		"push r24				\n\t"
		"push r25				\n\t"
	);

	SetTask_isr(TASK_PTR(TSK_TR_SRV));
	
	asm volatile (
		"pop  r25				\n\t"
		"pop  r24				\n\t"
		"pop  r0				\n\t"
		"out  __SREG__, r0		\n\t"
		"pop  r0				\n\t"
		"pop  r1				\n\t"
		"reti					\n\t"
	);
}
*/

ISR(TIMER2_COMPA_vect) {
	if(UNET_PORT_RO & _BV(UNET_PIN))
		UNET_REG |= UNET_PIN_MASK;
	else
		UNET_REG &= ~UNET_PIN_MASK;
	inc_sample_cnt_macro
	if(UNET_REG & ST_WRITE_MASK) {
		if(UNET_REG & UNET_WR_PIN_MASK)
			turn_write_macro
		else
			turn_read_macro
	}
	TASK_PTR(TSK_UNET_HANDLE)->active = 1;
}

/*
ISR(TIMER2_COMPA_vect, ISR_NAKED) {
	
	asm volatile (
		"push r1				\n\t"
		"push r0				\n\t"
		"in   r0, __SREG__		\n\t"
		"push r0				\n\t"
		"eor  r1, r1			\n\t"
		"push r24				\n\t"
		"push r25				\n\t"
		"in	  r24, %0			\n\t"
		"andi r24, %1			\n\t"
		"sts  unet_pin_st, r24	\n\t"
		:: "I"(_SFR_IO_ADDR(UNET_PORT_RO)), "I"(_BV(UNET_PIN))
	);
	
	//unet_pin_st = UNET_PORT_RO & _BV(UNET_PIN);
	//TASK_PTR(TSK_UNET_HANDLE)->active = 1;
	//SetTask_isr(TASK_PTR(TSK_UNET_HANDLE)); 
	//Enable_Interrupt
	//unet_timer_handle();
		
	asm volatile (
		"pop  r25				\n\t"
		"pop  r24				\n\t"
		"pop  r0				\n\t"
		"out  __SREG__, r0		\n\t"
		"pop  r0				\n\t"
		"pop  r1				\n\t"
		"reti					\n\t"
	);
	
}
*/

// Прерывание TWI (I2C)
//ISR(TWI_vect) {
//}

/* USART Rx Complete */
ISR(USART_RX_vect) {
	// принудительно записываем в буфер,
	// если в буфере нет места. первый элемент
	// будет удален
	q_uart_in.arr[q_uart_in.offset_set] = UDR;
	q_uart_in.offset_set = (q_uart_in.offset_set + 1) & (UART_BUFFER_SIZE - 1);
//	if(q_uart_in.offset_set == q_uart_in.offset_get)
		//q_uart_in.offset_get = (q_uart_in.offset_get + 1) & (UART_BUFFER_SIZE - 1);
}

/* USART, Data Register Empty */
ISR(USART_UDRE_vect) {
	UDR = q_uart_out.arr[q_uart_out.offset_get];
	q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);
	// очередь пуста, запрещаем прерывания по опустошению буфера передачи UDR UART
	if(q_uart_out.offset_get == q_uart_out.offset_set)
		Disable_UART_Int(UDRIE_MASK);
}

/* USART Tx Complete */
ISR(USART_TX_vect) {
	
}

void write_to_uart_buf(const uint8_t *arr, uint8_t sz) {
	uint8_t i;
	for(i = 0; i < sz; i++)
	{
		uart_log(arr[i]);
	}
}

// ------------------------------------------------------------------
// вывод символа в последовательный порт
void uart_log(uint8_t b) {
	uint8_t sreg = STATUS_REG;
	Disable_Interrupt
	Disable_UART_Int(UDRIE_MASK);
	STATUS_REG = sreg;
	
	q_uart_out.arr[q_uart_out.offset_set] = b;
	q_uart_out.offset_set = (q_uart_out.offset_set + 1) & (UART_BUFFER_SIZE - 1);
	if(q_uart_out.offset_set == q_uart_out.offset_get)
		q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);

	Enable_UART_Int(UDRIE_MASK);
}

void unet_handlr(const t_unet_pckt *ppckt) {
	uint8_t b;
	t_unet_pckt pckt;
	switch(ppckt->cmd) {
	case C_PING_RQ:			// запрос пинг
		// формируем ответное сообщение
		//uart_log('P');
		pckt.addr_dst = ppckt->addr_src;
		pckt.addr_src = UNET_ADDR;
		pckt.cmd = C_PING_RP;
		b = prot_make_RTRMD(ppckt->addr_dst, ppckt->addr_src, &tmp_arr_out[0], PROT_C_PING_RP, 0);
		write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
		if(!unet_sendb(ppckt->addr_src, pckt.cmd))
		{
			if(!unet_enque(&pckt))
			{
				//uart_log('F');
				// TODO
			}
		}
	break;
	case C_PING_RP:			// ответ пинг
		//uart_log('R');
		b = prot_make_RTRMD(ppckt->addr_dst, ppckt->addr_src, &tmp_arr_out[0], PROT_C_PING_RP, 0);
		write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
	break;
	case C_ACCPT:
		//uart_log('A');
		// TODO
	break;
	case C_ALM_W:			// датчик протечек тревога
		b = prot_make_RTRMD(ppckt->addr_dst, ppckt->addr_src, &tmp_arr_out[0], PROT_C_ALM_W, 0);
		write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
	break;
	case C_DATA:
	break;
	case C_NONE:
	default:
	break;
	}
}

void VirtualWire_handle(const uint8_t* buf, uint8_t len) {
}

void IdleTask(void) {
}

/*
void UnetTask(void)  {
	unet_timer_handle();
}
*/

// ------------------------------------------------------------------
// задача пользовательской обработки предложения протокола обмена
void protParseItem(void) {
	prot_item();
    // меняем обработчик, накапливаем данные для разбора
	TASK_PTR(TSK_PROT)->hdlr = protCollectTask;
    // отложенный вызов задачи
	SetTimerTask(TASK_PTR(TSK_PROT), 10);
}

// ------------------------------------------------------------------
// задача разбора предложения
void protParseSentTask(void) {
	if(parse_sentence()) {
        // меняем обработчик, вызываем установленный обработчик предложения
		TASK_PTR(TSK_PROT)->hdlr = protParseItem;
	}
	else {
        // меняем обработчик, продолжаем накапливать данные
		TASK_PTR(TSK_PROT)->hdlr = protCollectTask;
	}
    SetTask(TASK_PTR(TSK_PROT));    // добавляем задачу на выполнение
}

// ------------------------------------------------------------------
// задача поиска предложений во входном буфере по протоколу обмена
void protParseTask(void) {
	if(prot_parse()) {
        // меняем обработчик на разбор предложения
		TASK_PTR(TSK_PROT)->hdlr = protParseSentTask;
	}
	else {
        // меняем обработчик, продолжаем накапливать данные
		TASK_PTR(TSK_PROT)->hdlr = protCollectTask;
	}
    SetTask(TASK_PTR(TSK_PROT));    // добавляем задачу на выполнение
}

// ------------------------------------------------------------------
// задача накопления входных данных по протоколу обмена
void protCollectTask(void) {
	uint8_t sreg;
	uint8_t count = 0;
	while(TRUE) {
		if((count < UART_BUFFER_SIZE) && (q_uart_in.offset_set != q_uart_in.offset_get)) {
			sreg = STATUS_REG;
			Disable_Interrupt
			q_prot_in.arr[q_prot_in.offset_set] = q_uart_in.arr[q_uart_in.offset_get];
			q_uart_in.offset_get = (q_uart_in.offset_get + 1) & (UART_BUFFER_SIZE - 1);
			STATUS_REG = sreg;
			q_prot_in.offset_set = (q_prot_in.offset_set + 1) % PROT_BUFFER_SIZE;
			++count;
			if(q_prot_in.offset_get == q_prot_in.offset_set) {
				break;
			}
		}
		else {
			break;
		}
	}
	if(count != 0) {
        //uart_log(0x30 + count);
		TASK_PTR(TSK_PROT)->hdlr = protParseTask;
		SetTask(TASK_PTR(TSK_PROT));
	}
	else
		SetTimerTask(TASK_PTR(TSK_PROT), 10);
}

void DiodeOnTask(void) {
	SwonLED(STATUS_LED);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOffTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}

void DiodeOffTask(void) {
	SwoffLED(STATUS_LED);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOnTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}

/*
void LEDTask(void) {

	OCR2A += ocr_rate;
	ocr_rate = rand()/(RAND_MAX/50) - 25;
	if(OCR2A < 50)
		OCR2A += 50;
	SetTimerTask(TASK_PTR(TSK_LED), 50);
}
*/

void UnetTestTask(void) {
	static uint8_t cnt = 0;
	uint8_t arr[3];
	arr[0] = C_DATA;
	arr[1] = 1;
	arr[2] = cnt;
	cnt++;

	if(!unet_send(UNET_BROADCAST_ADDR, arr, 3)) {
		uart_log('f');
	}
	SetTimerTask(TASK_PTR(TSK_DEV_PRESENT), 5000);
}

void DevicePresentsTask(void) {
	//uint8_t len;
	//len = prot_make_RTRMD(0xFF, UNET_ADDR, &tmp_arr_out[0], PROT_C_PING_RP, 0);
	//write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], len);

	TASK_PTR(TSK_DEV_PRESENT)->hdlr = UnetTestTask;
	SetTimerTask(TASK_PTR(TSK_DEV_PRESENT), 5000);
}


/*
void PingTask(void) {
	unet_sendb(0xFF, C_PING_RQ);
	SetTimerTask(TASK_PTR(TSK_PING), 1000);
}
*/

/*
void rf_unet_send(const t_unet_pckt *pckt) {
	rf_buf[0] = 'U';
	memcpy(rf_buf + 1, pckt, 3);
	vw_send(rf_buf, 4);
}
*/

void proc_cmd_own(uint8_t addr_src, uint8_t cmd) {

	uint8_t b;

	switch(cmd) {
		case PROT_C_PING_RQ:
			b = prot_make_RTRMD(addr_src, UNET_ADDR, &tmp_arr_out[0], PROT_C_PING_RP, cmd);
			write_to_uart_buf((const uint8_t *)&tmp_arr_out[0], b);
		break;
		default:
		break;
	}
}

void proc_cmd_other(uint8_t addr_trg, uint8_t addr_src, uint8_t cmd) {

	t_unet_pckt pckt;
	pckt.addr_dst = addr_trg;
	pckt.addr_src = UNET_ADDR;
	
	switch(cmd) {
		case PROT_C_PING_RQ:
			pckt.cmd = C_PING_RQ;
			if(!unet_sendb(pckt.addr_dst, pckt.cmd)) {
				if(!unet_enque(&pckt)) {
					//uart_log('F');
				}
				else {
					//uart_log('q');
				}
			}
			else {
				//uart_log('p');
			}
		break;
		case PROT_C_PING_RP:
			//uart_log('P');
		break;
		case PROT_C_SWON_L:
			pckt.cmd = C_SWON_L;
			//rf_unet_send(&pckt);
			if(!unet_sendb(pckt.addr_dst, pckt.cmd)) {
				if(!unet_enque(&pckt)) {
					uart_log('F');
					// TODO
				}
				else
					uart_log('q');
			}
			else
				uart_log('p');
		break;
		case PROT_C_SWOFF_L:
			pckt.cmd = C_SWOFF_L;
			//rf_unet_send(&pckt);
			if(!unet_sendb(pckt.addr_dst, pckt.cmd)) {
				if(!unet_enque(&pckt)) {
					uart_log('F');
					// TODO
				}
				else
					uart_log('q');
			}
			else
				uart_log('p');
		break;
		default:
			//uart_log('U');
		break;
	}
}

// ------ Parsing sentences -----------------------------------------
BOOL parse_CNSMD(const uint8_t *arr, size_t sz)
{
	uint8_t addr_rcv;
	uint8_t addr_frm;
	uint8_t cmd;
	/* $PCNSMD,02,01,0F*21\r\n
     *  |   |  |  |  |  | |
	 *  |   |  |  |  |  | +- end sentence 
     *  |   |  |  |  |  +--- checksum
     *  |   |  |  |  +------ command type
     *  |   |  |  +--------- address of sender
     *  |   |  +------------ address of receiver
     *  |   +--------------- type of sentence (header part)
     *  +------------------- id of sender (header part)
     */
	if(sz != 21)
        return FALSE;
	
	addr_rcv = (ascii2hex(arr[8]) << 4) & 0xF0;
	addr_rcv |= ascii2hex(arr[9]) & 0x0F;
	
	addr_frm = (ascii2hex(arr[11]) << 4) & 0xF0;
	addr_frm |= ascii2hex(arr[12]) & 0x0F;
	
	cmd = (ascii2hex(arr[14]) << 4) & 0xF0;
	cmd |= ascii2hex(arr[15]) & 0x0F;
	
	// обработка команд предназначенных собственному устройству
	if((UNET_ADDR == addr_rcv) || (UNET_BROADCAST_ADDR == addr_rcv)) {
		proc_cmd_own(addr_frm, cmd);
	}
	
	// обработка команд для ретрансляции
	if((UNET_BROADCAST_ADDR == addr_rcv) || (UNET_ADDR != addr_rcv)) {
		proc_cmd_other(addr_rcv, addr_frm, cmd);
	}
	return TRUE;
}

// ------------------------------------------------------------------
BOOL parse_CMSMD(const uint8_t *arr, size_t sz) {

    return parse_CNSMD(arr, sz);
}

// ------------------------------------------------------------------
void InitAll(void)
{
	//t_vw_setup vw_prm;
	// init buffers
	q_uart_out.arr = arr_uart_out;
	q_uart_out.offset_set = 0;
    q_uart_out.offset_get = 0;
	q_uart_in.arr = arr_uart_in;
	q_uart_in.offset_set = 0;
    q_uart_in.offset_get = 0;
	//InitUSART
	// set baudrate
	UBRRL = LO(bauddivider);
	UBRRH = HI(bauddivider);
	// 
	UCSRA = 0;
	// enable receiver and transmiter
	UCSRB = _BV(RXEN)|_BV(TXEN);
	Disable_UART_Int(TXCIE_MASK|UDRIE_MASK);
	Enable_UART_Int(RXCIE_MASK);
	UCSRC = /*1<<URSEL|*/1<<UCSZ0|1<<UCSZ1;
	// Timer 0
	TCCR0A = 1<<WGM01;				// CTC Mode ( Автосброс после достижения регистра сравнения)
	TCCR0B = TCCR0B_PRESCALER;	    // Prescaler
	TCNT0 = 0;						// Установить начальное значение счётчиков
	OCR0A  = TimerDivider; 			// Установить значение в регистр сравнения
	TIMSK0 |= _BV(OCIE0A);			// Разрешаем прерывание RTOS - запуск ОС
	// Timer 2 (for unnet purpose)
	TCCR2A = _BV(WGM21);			// CTC Mode
	TCCR2B = UNET_PRESC;		    // Prescaler
	TCNT2  = 0;						// Установить начальное значение счётчиков
	OCR2A  = UNET_COMP;				// Установить значение в регистр сравнени
	TIMSK2 |= _BV(OCIE2A);
	// PWM 2
//	TCCR2A = _BV(WGM21) | _BV(WGM20) | _BV(COM2A1);	// Fast PWM, Clear on match
//	TCCR2B = TCCR2B_PRESC_1024;
//	TCNT2 = 0;
//	OCR2A  = 1;
	//
	//RF_DDR |= (1 << VW_TX_PIN);
	// led
	LED_DDR |= 1 << STATUS_LED;		// 
	//DDRB |= 1 << 3;
	//PORTB &= ~_BV(4);
	//DDRB |= 1 << 4;
	//PORTB &= ~_BV(5);
	//DDRB |= 1 << 5;
	
	// VirtualWire, Initialise the IO and ISR
	//vw_prm.pddr_rx = &DDRD;
	//vw_prm.pddr_tx = &RF_DDR;
	//vw_prm.ready_rx_handle = VirtualWire_handle;
    //vw_setup(2000, &vw_prm);      // Bits per sec
	//vw_rx_start();       // Start the receiver PLL running
	// Unet task
	if(!unet_init(unet_handlr))
		SwonLED(LED_FAIL);
	prot_init(UNET_ADDR, &sents[0], SENT_SIZE);
}

// ------------------------------------------------------------------
int main(void)
{
	InitAll();
	InitRTOS();
	tm_reg_task(TSK_DIOD, DiodeOnTask);
	tm_reg_task(TSK_DEV_PRESENT, DevicePresentsTask);
	//tm_reg_task(TSK_LED, LEDTask);
	tm_reg_task(TSK_PROT, protCollectTask);
	//tm_reg_task(TSK_PING, PingTask);
	tm_reg_task(TSK_UNET_DEQUEUE, unet_deque_task);
	tm_reg_task(TSK_UNET_RECEIVE, receive_func);
	tm_reg_task(TSK_UNET_HANDLE, unet_timer_handle);
	//tm_reg_task(TSK_VW_PLL, vw_pll);
	//
	// Diod task
	//SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
	// LED task
	//SetTimerTask(TASK_PTR(TSK_LED), 10);
	// prot
	sents[0].header = "PCNSMD";
	sents[0].func = parse_CNSMD;
    sents[1].header = "PCMSMD";
    sents[1].func = parse_CMSMD;
	
	//SetTimerTask(TASK_PTR(TSK_PROT), 10);
	// ping test
	//SetTimerTask(TASK_PTR(TSK_PING), 2000);
	// device present
	SetTimerTask(TASK_PTR(TSK_DEV_PRESENT), 1000);
	Enable_Interrupt				// разрещаем прерывания
	tm_exec();
	return 0;
}
