/*
 * RTOS.c
 *
 * Created: 11.03.2014 22:11:35
 *  Author: Asus
 */ 

#include "config.h"
#include "types.h"
#include "task_mngr.h"
#include "queuebuf.h"
#include "unnet.h"
#include "routing.h"

#define LED_FAIL RGB_LED_RED
#define UART_BUFFER_SIZE 16	// use &

// массив байт UART на отправку
static uint8_t arr_uart_out[UART_BUFFER_SIZE];
// массив байт, считанных с UART
static uint8_t arr_uart_in[UART_BUFFER_SIZE];

static volatile t_que q_uart_out;
static volatile t_que q_uart_in;

static volatile uint8_t b_uart_out;

#define SwonLED(led) LED_DDR |= _BV(led);	 	/* логический ноль  */
#define SwoffLED(led) LED_DDR &= ~(1 << led);	/* режим Hi-Z */

void swon_unet_led(void) {
	SwonLED(RGB_LED_BLUE);
}

void swoff_unet_led(void) {
	SwoffLED(RGB_LED_BLUE);
}

extern void DiodeOffTask(void);
extern void uart_log(uint8_t);

//RTOS Interrupt
ISR(RTOS_ISR) {
	TASK_PTR(TSK_TR_SRV)->active = 1;
}
/*
ISR(RTOS_ISR, ISR_NAKED) {
	asm volatile (
		"push r1				\n\t"
		"push r0				\n\t"
		"in   r0, __SREG__		\n\t"
		"push r0				\n\t"
		"eor  r1, r1			\n\t"
		"push r24				\n\t"
		"push r25				\n\t"
		"in	  r24, %0			\n\t"
		"andi r24, %1			\n\t"
		"sts  unet_pin_st, r24	\n\t"
		:: "I"(_SFR_IO_ADDR(UNET_PORT_RO)), "I"(_BV(UNET_PIN))
		
	);

	//unet_pin_st = UNET_PORT_RO & _BV(UNET_PIN);
	SetTask_isr(TASK_PTR(TSK_TR_SRV));
	
	asm volatile (
		"pop  r25				\n\t"
		"pop  r24				\n\t"
		"pop  r0				\n\t"
		"out  __SREG__, r0		\n\t"
		"pop  r0				\n\t"
		"pop  r1				\n\t"
		"reti					\n\t"
	);
}
*/

ISR(TIMER2_COMPA_vect) {
	unet_pin_st = UNET_PORT_RO & _BV(UNET_PIN);
	TASK_PTR(TSK_UNET_HANDLE)->active = 1;
	curr_num = 0;
	//SetTask_isr(TASK_PTR(TSK_UNET_HANDLE));
}

/* USART Rx Complete */
ISR(USART_RX_vect) {
	// принудительно записываем в буфер,
	// если в буфере нет места. первый элемент
	// будет удален
	q_uart_in.arr[q_uart_in.offset_set] = UDR;
	q_uart_in.offset_set = (q_uart_in.offset_set + 1) & (UART_BUFFER_SIZE - 1);
}

/* USART, Data Register Empty */
ISR(USART_UDRE_vect) {
	UDR = q_uart_out.arr[q_uart_out.offset_get];
	q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);
	// очередь пуста, запрещаем прерывания по опустошению буфера передачи UDR UART
	if(q_uart_out.offset_get == q_uart_out.offset_set)
		Disable_UART_Int(UDRIE_MASK);
}

/* USART Tx Complete */
ISR(USART_TX_vect) {
	
}

void write_to_uart_buf(const uint8_t *arr, uint8_t sz) {
	uint8_t i;
	for(i = 0; i < sz; i++)
	{
		uart_log(arr[i]);
	}
}

void uart_log(uint8_t b) {
	uint8_t sreg = STATUS_REG;
	Disable_Interrupt
	
	q_uart_out.arr[q_uart_out.offset_set] = b;
	q_uart_out.offset_set = (q_uart_out.offset_set + 1) & (UART_BUFFER_SIZE - 1);
	if(q_uart_out.offset_set == q_uart_out.offset_get)
		q_uart_out.offset_get = (q_uart_out.offset_get + 1) & (UART_BUFFER_SIZE - 1);

	STATUS_REG = sreg;
	Enable_UART_Int(UDRIE_MASK);
}

void unet_handlr(const t_unet_pckt *ppckt) {
	t_unet_pckt pckt;
	switch(ppckt->cmd) {
	case C_PING_RQ:			// запрос пинг
		// формируем ответное сообщение
		pckt.addr_dst = ppckt->addr_src;
		pckt.addr_src = UNET_ADDR;
		pckt.cmd = C_PING_RP;
		if(!unet_send(ppckt->addr_src, &pckt.cmd, 1))
		{
			if(!unet_enque(&pckt))
			{
				// TODO
			}
		}
	break;
	case C_PING_RP:			// ответ пинг

	break;
	case C_ACCPT:
		// TODO
	break;
	case C_ALM_W:			// датчик протечек тревога

	break;
	case C_DATA:
	break;
	case C_NONE:
	default:
		//SwonLED(RGB_LED_RED);
	break;
	}
}

void DiodeOnTask(void) {
	//swoff_unet_led_macro
	//SwoffLED(RGB_LED_RED);
	SwonLED(RGB_LED_GREEN);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOffTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}

void DiodeOffTask(void) {
	//swoff_unet_led_macro
	//SwoffLED(RGB_LED_RED);
	SwoffLED(RGB_LED_GREEN);
	TASK_PTR(TSK_DIOD)->hdlr = DiodeOnTask;
	SetTimerTask(TASK_PTR(TSK_DIOD), 1000);
}

void CheckWaterSensorsTask(void) {
	if(!(PINC & _BV(WATER_SENS_ALARM)))
	{
		uint8_t b = C_ALM_W;
		SwonLED(LED_FAIL);
		unet_send(UNET_BROADCAST_ADDR, &b, 1);
	}
	SetTimerTask(TASK_PTR(TSK_CHK_W_SENS), 1000);
}

/*
void PingTask(void) {
	uint8_t b = C_PING_RQ;
	if(!unet_send(0x02, &b, 1)) {
		SetTimerTask(TASK_PTR(TSK_PING), 1000);
	}
}
*/

void DevicePresentsTask(void) {
	t_unet_pckt pckt;
	pckt.addr_dst = UNET_BROADCAST_ADDR;
	pckt.addr_src = UNET_ADDR;
	pckt.cmd = C_PING_RP;
	if(!unet_send(pckt.addr_dst, &pckt.cmd, 1)) {
		if(!unet_enque(&pckt)) {
			SetTimerTask(TASK_PTR(TSK_DEV_PRESENT), 1000);
		}
	}
}

// ------------------------------------------------------------------
void InitAll(void)
{
	// init buffers
	q_uart_out.arr = arr_uart_out;
	q_uart_out.offset_set = 0;
    q_uart_out.offset_get = 0;
	q_uart_in.arr = arr_uart_in;
	q_uart_in.offset_set = 0;
    q_uart_in.offset_get = 0;
	//InitUSART
	// set baudrate
	UBRRL = LO(bauddivider);
	UBRRH = HI(bauddivider);
	// 
	UCSRA = 0;
	// enable receiver and transmiter
	UCSRB = 1<<RXEN|1<<TXEN;
	Disable_UART_Int(TXCIE_MASK|UDRIE_MASK);
	Enable_UART_Int(RXCIE_MASK);
	UCSRC = /*1<<URSEL|*/1<<UCSZ0|1<<UCSZ1;
	// задаем питание RGB-светодиоду
//	LED_PORT |= 1 << RGB_LED_GREEN;		// режим pull-up
//	LED_PORT |= 1 << RGB_LED_BLUE;		// режим pull-up
//	LED_PORT |= 1 << RGB_LED_RED;		// режим pull-up
	PORTC |= 1 << WATER_SENS_ALARM;     // pull-up
	PORTC |= 1 << WATER_SENS_ALARM_C;   // pull-up
	DDRC &= ~(1<< WATER_SENS_ALARM);
	DDRC &= ~(1<< WATER_SENS_ALARM_C);
	// Timer 0
	TCCR0A = 1<<WGM01;				// CTC Mode Автосброс после достижения регистра сравнения
	TCCR0B = TCCR0B_PRESCALER;		// Prescaler:
	TCNT0 = 0;						// Установить начальное значение счётчиков
	OCR0A  = TimerDivider; 			// Установить значение в регистр сравнения
	TIMSK0 |= 1<<OCIE0A;			// Разрешаем прерывание RTOS - запуск ОС
	// Timer 2
	TCCR2A = _BV(WGM21);			// CTC Mode
	TCCR2B = TCCR2B_PRESC_8;		// Prescaler
	TCNT2  = 0;
	OCR2A  = F_CPU/8/10000L;
	TIMSK2 |= _BV(OCIE2A);
	// UnNet task
	if(!unet_init(unet_handlr))
		SwonLED(LED_FAIL);
}

int main(void)
{
	InitAll();
	InitRTOS();			//
	tm_reg_task(TSK_DIOD, DiodeOnTask);
	tm_reg_task(TSK_DEV_PRESENT, DevicePresentsTask);
	//tm_reg_task(TSK_PING, PingTask);
	tm_reg_task(TSK_CHK_W_SENS, CheckWaterSensorsTask);
	tm_reg_task(TSK_UNET_DEQUEUE, unet_deque_task);
	tm_reg_task(TSK_UNET_RECEIVE, receive_func);
	tm_reg_task(TSK_UNET_HANDLE, unet_timer_handle);

	// WaterSensorAlarm task
	SetTimerTask(TASK_PTR(TSK_CHK_W_SENS), (uint16_t)1000);
	// Diod task
	SetTimerTask(TASK_PTR(TSK_DIOD), (uint16_t)1000);
	// device present
	SetTimerTask(TASK_PTR(TSK_DEV_PRESENT), (uint16_t)1000);

	Enable_Interrupt
	tm_exec();
	return 0;
}