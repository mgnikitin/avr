
#define UNET_ADDR 0x03

// Port B
#define UNET_PORT PORTB
#define UNET_PORT_RO PINB
#define UNET_DDR DDRB
#define UNET_PIN 1
// Port C
#define WATER_SENS_ALARM 5
#define WATER_SENS_ALARM_C 4
// Port D
#define LED_DDR DDRD
#define LED_PORT PORTD
#define LED_PORT_RO PIND
#define RGB_LED_GREEN 7
#define RGB_LED_BLUE 5
#define RGB_LED_RED 6
//

// 0 prohibited
#define TSK_DIOD	  	1
#define TSK_UNET_DEQUEUE 2
#define TSK_UNET_RECEIVE 3
#define TSK_UNET_HANDLE  4
#define TSK_CHK_W_SENS  5
#define TSK_DEV_PRESENT 6
//#define TSK_PING		5
